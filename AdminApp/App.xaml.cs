﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using AdminApp.Views;
using System.Threading.Tasks;

namespace AdminApp
{
    public partial class App : Application
    {
        //private LoginViewModel loginView;

        public static string Filepath;
        public App()
        {
            InitializeComponent();
            MainPage = new NavigationPage(new MySplashScreen());
        }   
        public App(string filepath)
        {
            InitializeComponent();

             MainPage = new NavigationPage(new MySplashScreen());

            Filepath = filepath;
        }

      


        protected override void OnStart()
        {
            // Handle when your app starts
       
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
