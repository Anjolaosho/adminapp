﻿using AdminApp.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AdminApp.MyInterfaces.IServices
{
    public interface IAuthenticationService
    {
        Task<AuthenticationResponse> Register(string nameoforg, string email, string password);

        Task<AuthenticationResponse> Authenticate(string email, string password);

        Task<TenantProfile> gettenant(string authtoken = "");

        bool IsUserAuthenticated();
    }
}
