﻿using AdminApp.Models.NoticeModel;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AdminApp.MyInterfaces.IServices.Data
{
    public interface INotices
    {
        Task<Notices> notices(Notices notices, string authtoken = "");

        Task<List<YourNotice>> GetAllNoticeAsync(string authtoken = "");

        Task DeleteEvent(YourNotice notices, string authtoken = "");
    }
}
