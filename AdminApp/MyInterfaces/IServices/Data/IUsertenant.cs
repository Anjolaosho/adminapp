﻿using AdminApp.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AdminApp.MyInterfaces.IServices.Data
{
    interface IUsertenant
    {
        Task<TenantUser> tenantUser(string Name, string Email, string authtoken);

        Task<List<TenantUser>> GetTenantUsers(string authtoken);
    }
}
