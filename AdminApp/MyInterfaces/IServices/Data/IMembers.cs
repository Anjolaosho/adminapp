﻿using AdminApp.Models.MembersModel;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AdminApp.MyInterfaces.IServices.Data
{
    public interface IMembers
    {
        Task<AddMember> addMember(AddMember addMember, string authtoken = "");

        Task<List<Member>> GetAllMembersAsync(string authtoken = "");

        Task DeleteMember(Member member, string authtoken = "");
    }
}
