﻿using AdminApp.Models.EventsModel;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AdminApp.MyInterfaces.IServices.Data
{
    interface IEvents
    {
        Task<Event> @event(Event @event, string authtoken = "");

        Task<List<Events>> GetAllEventsAsync(string authtoken = "");

        Task DeleteEvent(Events @event, string authtoken = "");
    }
}
