﻿using AdminApp.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AdminApp.MyInterfaces.IServices.Data
{
    interface IBanks
    {
        Task<List<BankData>> banks(string authtoken);
    }
}
