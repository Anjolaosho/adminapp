﻿using AdminApp.Models.MembersModel;
using AdminApp.Models.PaymentsModel;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AdminApp.MyInterfaces.IServices.Data
{
    interface IPayments
    { 
      Task<AddPayment> payments(AddPayment payment, string authtoken = "");
      Task<List<PaymentRecords>> GetPaymentsAsync(Member member, PaymentRecords payment,string authtoken = "");

      Task<List<Payment>> GetAllpaymentPlans(string authtoken = "");

       Task<List<MainSubscription>> GetSubscriptions(string authtoken = "");

        Task<List<MainSubscription>> GetExpiredsubscriptions(string authtoken = "");

        Task<List<MainSubscription>> GetActivesubscriptions(string authtoken = "");

        Task<MainSub> MakePayment(MainSub main, string authtoken = "");

        Task DeletePayment(Payment payment, string authtoken = "");
    }
}
