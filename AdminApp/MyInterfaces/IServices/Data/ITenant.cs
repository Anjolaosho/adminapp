﻿using AdminApp.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace AdminApp.MyInterfaces.IServices.Data
{
    interface ITenant
    {
        Task<UpdateTenant> updateTenant(DateTime dateEstablished, string nameOfOrganization, string email, string accountNumber = "",
                string bank = "", string address = "", AzureImageObject azureImage = null,  string authtoken = "");
    }
}
