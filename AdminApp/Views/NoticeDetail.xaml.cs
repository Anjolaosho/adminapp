﻿using AdminApp.Models.NoticeModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AdminApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class NoticeDetail : ContentPage
    {
        public NoticeDetail(string token ,YourNotice yourNotice)
        {
            InitializeComponent();
            BindingContext = yourNotice;
        }
    }
}