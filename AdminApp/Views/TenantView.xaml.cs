﻿using AdminApp.Container;
using AdminApp.Models.MembersModel;
using AdminApp.Models.PaymentsModel;
using AdminApp.MyInterfaces.IServices.Data;
using AdminApp.Repository;
using AdminApp.Services.Data;
using AdminApp.ViewModels.TenantViewModel;
using PCLStorage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AdminApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TenantView : ContentPage
    {
        private readonly IPayments payments;
        IFolder rootFolder = FileSystem.Current.LocalStorage;
        private string Token;
        private List<MainSubscription> myActivesub = new List<MainSubscription>();
        private List<MainSubscription> myExpiredsub = new List<MainSubscription>();
        private List<Member> MemberAmountofMembers = new List<Member>();
        private readonly IMembers members;
        public TenantView(string authenticationResponse,string bank)
        {
            InitializeComponent();

            Token = authenticationResponse;
            members = new MemberServices(new GenericRepository());
            payments = new PaymentServices(new GenericRepository());
            BindingContext = new TenantViewModel(authenticationResponse,bank,this.Navigation);
        }

        private async void Button_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new TenantProfile(Token));
        }

        private async void Button_Clicked_1(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new TenantUser(Token));
        }

        public async Task<List<MainSubscription>> InitializeAsync()
        {
            IsBusy = true;
            try
            {
                var GetActivesub = await payments.GetActivesubscriptions(Token);

                IsBusy = false;
                //   myevents = getevents;
                return GetActivesub;
            }
            catch
            {
                var GetActivesub = await payments.GetActivesubscriptions(Token);

                IsBusy = false;
                //   myevents = getevents;
                return GetActivesub;
            }
          
        }

        public async Task<List<MainSubscription>> InitializeExpiredAsync()
        {
            IsBusy = true;
            try
            {
                var GetExpiredsub = await payments.GetExpiredsubscriptions(Token);

                IsBusy = false;
                //   myevents = getevents;
                return GetExpiredsub;
            }
            catch
            {
                var GetExpiredsub = await payments.GetExpiredsubscriptions(Token);

                IsBusy = false;
                //   myevents = getevents;
                return GetExpiredsub;
            }
           
        }

        public async Task<List<Member>> InitializeMemberAsync()
        {
            IsBusy = true;
            try
            {
                var getmembers = await members.GetAllMembersAsync(Token);

                IsBusy = false;
                return getmembers;
            }
            catch
            {
                var getmembers = await members.GetAllMembersAsync(Token);

                IsBusy = false;
                return getmembers;
            }
         
        }


        protected async override void OnAppearing()
        {
            myExpiredsub = await InitializeExpiredAsync();
            myActivesub = await InitializeAsync();
            MemberAmountofMembers = await InitializeMemberAsync();
            var MemAmount = MemberAmount(MemberAmountofMembers);
            var money = Addup(myActivesub);
            var nomoney = Addup(myExpiredsub);
            var expiresoon = Soontoexpire(myActivesub);
            Allmoney.Text = money.ToString();
            Allexpired.Text = nomoney.ToString();
            Allexpiring.Text = expiresoon.ToString();
            AllMembers.Text = MemAmount.ToString();
            getimage();
            base.OnAppearing();
        }

        private long Addup(List<MainSubscription> money)
        {
            long sum = 0;

            foreach (var cash in money)
            {
                sum += cash.AmountPaid;
            }

            return sum;
        }  
        
        
        private long MemberAmount(List<Member> Amem)
        {
            int sum = Amem.Count;

            return sum;
        }

        private long Soontoexpire(List<MainSubscription> money)
        {
            
            long sum = 0;

            foreach (var cash in money)
            {
                if(cash.DaysToExpire <= 7 && cash.DaysToExpire > 0)
                {
                    sum += cash.AmountPaid;
                }
                
            }

            return sum;
        }


        private async void getimage()
        {
            try
            {
                var myfile = await rootFolder.GetFolderAsync("Pipu");

                if (await PCLHelper.IsFileExistAsync("AuthFile", myfile))
                {
                    orgname.Text = await PCLHelper.ReadAllTextAsync("AuthFile", myfile);
                }

                if (await PCLHelper.IsFileExistAsync("Logo", myfile))
                {
                    // await myfile.GetFileAsync("Logo");
                    var theimage = await PCLHelper.LoadImage(new byte[0], "Logo", myfile); //new byte[96],
                    var img = Encoding.ASCII.GetString(theimage);
                    UserImage.Source = img;

                    //  Thread.Sleep(5000);

                }
            }
            catch
            {
                await DisplayAlert("", "Image couldnt be loaded", "ok");
            }
          

        }

    

        private async void Button_Clicked_2(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new ActiveMembers(Token));
        }

        private async void Button_Clicked_3(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new ExpiredMembers(Token));
        }


    }
}