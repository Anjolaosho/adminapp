﻿using AdminApp.MyInterfaces.IRepository;
using AdminApp.MyInterfaces.IServices;
using AdminApp.MyInterfaces.IServices.General;
using AdminApp.Repository;
using AdminApp.Services;
using AdminApp.Services.Data;
using AdminApp.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AdminApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Login : ContentPage
    {
        public IAuthenticationService authenticationService { get; }
        
        public IDialogService Dialog { get; }


        public IGenericRepository genericRepository { get; }
        public IConnectionService connectionService { get; }
        public Login(string email = "" , string password = "", string nameoforg = "")
        {
            InitializeComponent();

            genericRepository = new GenericRepository();
            authenticationService = new ApiServices(genericRepository);
           connectionService = new ConnectionService();

        BindingContext = new LoginViewModel(this.authenticationService, this.Navigation,this.Dialog, connectionService, email, password);

            Mypassword.IsPassword = true;
        }

        private void Button_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new ForgotPassword());
        }

        private void TapGestureRecognizer_Tapped(object sender, EventArgs e)
        {
            Mypassword.IsPassword = Mypassword.IsPassword ? false : true;

            passwordview.Source = Mypassword.IsPassword ? "closedeye.png" : "eyeopen.png";
        }
    }
}