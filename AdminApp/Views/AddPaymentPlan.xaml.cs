﻿using AdminApp.Container;
using AdminApp.Models;
using AdminApp.MyInterfaces.IServices.Data;
using AdminApp.MyInterfaces.IServices.General;
using AdminApp.Repository;
using AdminApp.Services;
using AdminApp.Services.Data;
using AdminApp.ViewModels.PaymentsViewModel;
using PCLStorage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AdminApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AddPaymentPlan : ContentPage
    {
        IFolder folder = FileSystem.Current.LocalStorage;
        private readonly IConnectionService connectionService;
        private readonly IPayments payments;
        private string Token;
        public AddPaymentPlan(string authenticationResponse)
        {
            InitializeComponent();

            connectionService = new ConnectionService();
            payments = new PaymentServices(new GenericRepository());
            Token = authenticationResponse;
            BindingContext = new AddPaymentViewModel(connectionService, this.Navigation, payments, authenticationResponse);
        }

        protected override void OnAppearing()
        {
            getimage();
        }

        private async void getimage()
        {
            IFolder rootFolder = FileSystem.Current.LocalStorage;

            var myfile = await rootFolder.GetFolderAsync("Pipu");
            if (await PCLHelper.IsFileExistAsync("Logo", myfile))
            {
                // await myfile.GetFileAsync("Logo");
                var theimage = await PCLHelper.LoadImage(new byte[0], "Logo", myfile); //new byte[96],
                var img = Encoding.ASCII.GetString(theimage);
                UserImage.Source = img;
              
            }

            if (await PCLHelper.IsFileExistAsync("AuthFile", myfile))
            {
                orgname.Text = await PCLHelper.ReadAllTextAsync("AuthFile", myfile);
            }

        }

    }
}