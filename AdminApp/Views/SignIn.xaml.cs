﻿using AdminApp.MyInterfaces.IRepository;
using AdminApp.MyInterfaces.IServices;
using AdminApp.MyInterfaces.IServices.General;
using AdminApp.Repository;
using AdminApp.Services;
using AdminApp.Services.Data;
using AdminApp.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AdminApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SignIn : ContentPage
    {
        public IAuthenticationService authenticationService { get; }

        public IConnectionService connectionService { get; }

       public IGenericRepository genericRepository { get;}

        public SignIn()
        {
            InitializeComponent();

            genericRepository = new GenericRepository();
           authenticationService = new ApiServices(genericRepository);
            connectionService = new ConnectionService();

          BindingContext = new RegistrationViewModel(authenticationService, this.Navigation, connectionService);

            entrypassword.IsPassword = true;
        }

        private async void Button_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new ForgotPassword());
        }

        private void TapGestureRecognizer_Tapped(object sender, EventArgs e)
        {
            entrypassword.IsPassword = entrypassword.IsPassword ? false : true;

            passwordview.Source = entrypassword.IsPassword ? "closedeye.png" : "eyeopen.png";
        }

        private void entrypassword_TextChanged(object sender, TextChangedEventArgs e)
        {
            Entry entry = sender as Entry;
            String val = entry.Text;
            if (val.Length >= 1)
            {
                Passtext.IsVisible = true;
            }
            else
            {
                Passtext.IsVisible = false;
            }

            if (val.Length > 5)
            {
                Passtext.IsVisible = false;
            }
            else
            {
                Passtext.TextColor = Color.Red;
            } 
        }
    }
}