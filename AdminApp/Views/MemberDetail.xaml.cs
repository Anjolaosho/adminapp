﻿using AdminApp.Models.MembersModel;
using Rg.Plugins.Popup.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AdminApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MemberDetail : ContentPage
    {
        private string Token;
        private Member member;
        public MemberDetail(string token, Member yourMember)
        {
            InitializeComponent();
            BindingContext = yourMember;
            Token = token;
            member = yourMember;
        }

        private async void Button_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new PaymentRecord(Token, member));
        }

        private async void Button_Clicked_1(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new SuscribeMember(Token, member.FullName));
        }

        private async void TapGestureRecognizer_Tapped(object sender, EventArgs e)
        {
            await Navigation.PushPopupAsync(new Page1(member.ProfilePictureBlobUrl,Token));
        }
    }
}