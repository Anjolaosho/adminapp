﻿using AdminApp.Container;
using AdminApp.Models;
using AdminApp.Models.EventsModel;
using AdminApp.MyInterfaces.IServices.Data;
using AdminApp.MyInterfaces.IServices.General;
using AdminApp.Repository;
using AdminApp.Services;
using AdminApp.Services.Data;
using AdminApp.ViewModels.EventsViewModel;
using PCLStorage;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AdminApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AllEventsTempelates : ContentPage
    {
        private readonly IConnectionService connectionService;
        private readonly IEvents events;
        public AuthenticationResponse _GetAuthenticationResponse { get; set; }

        private string token;
        private List<Events> myevents = new List<Events>();
        private List<Events> alltheevents = new List<Events>();

        private ObservableCollection<Events> getEvents;
        public ObservableCollection<Events> GetEvents { get; set; }
        public AllEventsTempelates(string authenticationResponse)
        {
            InitializeComponent();

        ///    _connection = DependencyService.Get<ISQLiteDb>().GetConnection();
            connectionService = new ConnectionService();
            events = new EventsServices(new GenericRepository());
            token = authenticationResponse;

            
            BindingContext = new AllEventsViewModel(connectionService, this.Navigation, events, authenticationResponse);
            


        }

        private async void TapGestureRecognizer_Tapped(object sender, EventArgs e)
        {
            var args = (TappedEventArgs)e;
            var myObject = args.Parameter;
            var item = (Events)myObject;
            var delete = await DisplayAlert("Delete event", "Delete this event?", "Yes","No");
            try
            {
                if (connectionService.IsConnected)
                {
                    if (delete)
                    {
                        await events.DeleteEvent(item, token);
                        await DisplayAlert("Delete event", "Event Deleted", "Ok");
                        getEvents.Remove(item);
                        await Navigation.PopAsync();
                    }
                }
                else
                {
                    await DisplayAlert("please check connection", "no connection etected", "Yes");
                }
            }
            catch
            {

            }
       
        }

        public async Task<List<Events>> InitializeAsync()
        {
            IsBusy = true;

            var getevents = await events.GetAllEventsAsync(token);

            IsBusy = false;
            //   myevents = getevents;
            return getevents;
        }


        protected async override void OnAppearing()
        {
            myevents = await InitializeAsync();

            //using (SQLiteConnection conn = new SQLiteConnection(App.Filepath))
            //{
            //    conn.CreateTable<Events>();
                
            //    conn.UpdateAll(myevents);
            //    alltheevents = conn.Table<Events>().ToList();
            //}

            getEvents = new ObservableCollection<Events>(myevents);
            eventlist.ItemsSource = getEvents;
            getimage();
            base.OnAppearing();
        }

        private void SearchBar_TextChanged(object sender, TextChangedEventArgs e)
        {
            eventlist.ItemsSource = GetContacts(e.NewTextValue);
        }

        IEnumerable<Events> GetContacts(string searchText = null)
        {

            if (string.IsNullOrEmpty(searchText))
            {
                return getEvents;
            }
            else
            {
                ObservableCollection<Events> events = new ObservableCollection<Events>(myevents); 
                return events.Where(p => p.Title.ToLower().Contains(searchText) || p.Title.ToUpper().Contains(searchText));
            }

        }

        private async void getimage()
        {
            IFolder rootFolder = FileSystem.Current.LocalStorage;

            var myfile = await rootFolder.GetFolderAsync("Pipu");
            if (await PCLHelper.IsFileExistAsync("Logo", myfile))
            {
                // await myfile.GetFileAsync("Logo");
                var theimage = await PCLHelper.LoadImage(new byte[0], "Logo", myfile); //new byte[96],
                var img = Encoding.ASCII.GetString(theimage);
                UserImage.Source = img;
            }

            if (await PCLHelper.IsFileExistAsync("AuthFile", myfile))
            {
                orgname.Text = await PCLHelper.ReadAllTextAsync("AuthFile", myfile);
            }

        }

        private async void eventlist_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            Events item = (Events)e.Item;
            ((ListView)sender).SelectedItem = null;
            await Navigation.PushAsync(new EventDetails(token, item));
        }
    }
}