﻿using AdminApp.Models.MembersModel;
using AdminApp.Models.PaymentsModel;
using AdminApp.MyInterfaces.IServices.Data;
using AdminApp.Repository;
using AdminApp.Services.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AdminApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PaymentRecord : ContentPage
    {
        private Member member;
        private string Token;
        private IPayments payment;
        public PaymentRecord(string token, Member yourMember)
        {
            InitializeComponent();

            member = yourMember;
            Token = token;
            payment = new PaymentServices(new GenericRepository());
            BindingContext = yourMember;
           
        }

        protected async override void OnAppearing()
        {
            base.OnAppearing();
            getrecords.ItemsSource = await payment.GetPaymentsAsync(member, new PaymentRecords(), Token);
        }

        private void getrecords_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            ((ListView)sender).SelectedItem = null;
        }

        private void TapGestureRecognizer_Tapped(object sender, EventArgs e)
        {
            Navigation.PopAsync();
        }
    }
}