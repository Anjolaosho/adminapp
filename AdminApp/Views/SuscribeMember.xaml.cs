﻿using AdminApp.Container;
using AdminApp.Models.MembersModel;
using AdminApp.Models.PaymentsModel;
using AdminApp.MyInterfaces.IRepository;
using AdminApp.MyInterfaces.IServices.Data;
using AdminApp.MyInterfaces.IServices.General;
using AdminApp.Repository;
using AdminApp.Services;
using AdminApp.Services.Data;
using AdminApp.ViewModels.PaymentsViewModel;
using PCLStorage;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AdminApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SuscribeMember : ContentPage
    {
        private readonly IGenericRepository _genericRepository;
        private readonly IConnectionService connectionService;
        private readonly IPayments _payments;
        private readonly IMembers members;
        private string token;
        private List<string> banks;
        private ObservableCollection<Payment> getallpayments;
        private ObservableCollection<Member> getallmembers;

        private List<Member> allmembers = new List<Member>();
        private List<Payment> allpayments = new List<Payment>();
        private List<string> allpayment = new List<string>();
        private List<string> allmember = new List<string>();


        public SuscribeMember(string authenticationResponse,string name = "")
        {
            InitializeComponent();

            token = authenticationResponse;
            connectionService = new ConnectionService();
            _genericRepository = new GenericRepository();
            _payments = new PaymentServices(new GenericRepository());
            members = new MemberServices(new GenericRepository());
            BindingContext = new SuscribeMemberViewModel( _genericRepository, connectionService, this.Navigation, _payments, authenticationResponse, name);

        }

        private async Task<List<Payment>> GetAllpayment()
        {
            var details = await _payments.GetAllpaymentPlans(token);

            return details;
        }

        private async Task<List<Member>> GetAllmember()
        {
            var details = await members.GetAllMembersAsync(token);

            return details;
        }

        protected async override void OnAppearing()
        {
            allmembers = await GetAllmember();
            allpayments = await GetAllpayment();

            //  getallbanks = new ObservableCollection<BankData>(allbanks);


              allmember = allmembers.Select(boss => boss.FullName).ToList();
            allpayment = allpayments.Select(boss => boss.Name).ToList();


            Memberlist.ItemsSource = allmembers;
            PaymentPlans.ItemsSource = allpayments;

            getimage();
        }
        private async void getimage()
        {
            IFolder rootFolder = FileSystem.Current.LocalStorage;

            var myfile = await rootFolder.GetFolderAsync("Pipu");
            if (await PCLHelper.IsFileExistAsync("Logo", myfile))
            {
                // await myfile.GetFileAsync("Logo");
                var theimage = await PCLHelper.LoadImage(new byte[0], "Logo", myfile); //new byte[96],
                var img = Encoding.ASCII.GetString(theimage);
                UserImage.Source = img;
            }

            if (await PCLHelper.IsFileExistAsync("AuthFile", myfile))
            {
                orgname.Text = await PCLHelper.ReadAllTextAsync("AuthFile", myfile);
            }

        }

    }
}