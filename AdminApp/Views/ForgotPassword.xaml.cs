﻿using AdminApp.MyInterfaces.IServices.Data;
using AdminApp.MyInterfaces.IServices.General;
using AdminApp.Repository;
using AdminApp.Services;
using AdminApp.Services.Data;
using AdminApp.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AdminApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ForgotPassword : ContentPage
    {
        private readonly IForgot forgot;
        private readonly IConnectionService connectionService;
        public ForgotPassword()
        {
            InitializeComponent();
            forgot = new ForgotPasswordServices(new GenericRepository());
        connectionService = new ConnectionService();
            BindingContext = new ForgotPasswordViewModel(connectionService, this.Navigation,forgot);
        }
    }
}