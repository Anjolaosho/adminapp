﻿using AdminApp.Models.MembersModel;
using AdminApp.Models.PaymentsModel;
using AdminApp.MyInterfaces.IServices.Data;
using AdminApp.MyInterfaces.IServices.General;
using AdminApp.Repository;
using AdminApp.Services;
using AdminApp.Services.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AdminApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MemberPayment : ContentPage
    {
        private readonly IConnectionService connectionService;
        private readonly IPayments payments;
        private string Token;
        private Member Member;
        public MemberPayment(string token, Member member)
        {
            InitializeComponent();

            Token = token;
            connectionService = new ConnectionService();
            payments = new PaymentServices(new GenericRepository());

            Member = member;
            memberpic.Source = member.ProfilePictureBlobUrl;

            membername.Text = member.FullName;

            paymentlist.ItemsSource = member.Tenant.Payments;

        }

        private async void Button_Clicked(object sender, EventArgs e)
        {
            await DisplayAlert("Payment Processing", "Payment is being sent ", "ok");
            

        }


        private async void makepayment()
        {
            if (connectionService.IsConnected)
            {
                MainSubscription mainSubscription = new MainSubscription()
                {
                     
                };

              //   await payments.MakePayment(mainSubscription, Token);
            }
        }
    }
}