﻿using AdminApp.Container;
using AdminApp.Models;
using AdminApp.Models.MembersModel;
using AdminApp.MyInterfaces.IServices.Data;
using AdminApp.MyInterfaces.IServices.General;
using AdminApp.Repository;
using AdminApp.Services;
using AdminApp.Services.Data;
using AdminApp.ViewModels.MembersViewModel;
using PCLStorage;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AdminApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AllMembersTempelate : ContentPage
    {
        private readonly IConnectionService connectionService;
        private readonly IMembers members;
        private string token;
        public AuthenticationResponse _GetAuthenticationResponse { get; set; }

        private List<Member> mymembers = new List<Member>();

        private ObservableCollection<Member> getMembers;
        public ObservableCollection<Member> GetEvents { get; set; }
        public AllMembersTempelate(string authenticationResponse)
        {
            InitializeComponent();

            connectionService = new ConnectionService();
            members = new MemberServices(new GenericRepository());
            token = authenticationResponse;
            BindingContext = new AllMembersViewModel(connectionService, this.Navigation, members, authenticationResponse);
       
        }
        private void TapGestureRecognizer_Tapped(object sender, EventArgs e)
        {
            DisplayAlert("Delete Member", "are you sure you want to delete member","Yes","No");
        }

        public async Task<List<Member>> InitializeAsync()
        {
            IsBusy = true;
            try
            {
                var getmembers = await members.GetAllMembersAsync(token);

                IsBusy = false;
                return getmembers;
            }
            catch
            {
                var getmembers = await members.GetAllMembersAsync(token);

                IsBusy = false;
                return getmembers;
            }  
         
           
        }

        protected async override void OnAppearing()
        {
            try
            {
                mymembers = await InitializeAsync();
                getMembers = new ObservableCollection<Member>(mymembers);
                allmembers.ItemsSource = getMembers;
                getimage();
            }
            catch(Exception e)
            {
                await DisplayAlert("Something went wrong", $"{e}", "Ok"); 
            }
           
        }

        private async void allmembers_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            Member yourMember = (Member)e.Item;
            ((ListView)sender).SelectedItem = null;
            await Navigation.PushAsync(new MemberDetail(token, yourMember));
        }

        private async void getimage()
        {
            IFolder rootFolder = FileSystem.Current.LocalStorage;

            var myfile = await rootFolder.GetFolderAsync("Pipu");
            if (await PCLHelper.IsFileExistAsync("Logo", myfile))
            {
                // await myfile.GetFileAsync("Logo");
                var theimage = await PCLHelper.LoadImage(new byte[0], "Logo", myfile); //new byte[96],
                var img = Encoding.ASCII.GetString(theimage);
                UserImage.Source = img;
            }

            if (await PCLHelper.IsFileExistAsync("AuthFile", myfile))
            {
                orgname.Text = await PCLHelper.ReadAllTextAsync("AuthFile", myfile);
            }

        }

        private void SearchBar_TextChanged(object sender, TextChangedEventArgs e)
        {
            allmembers.ItemsSource = GetContacts(e.NewTextValue);
        }

        // To get the value input in the search bar and query the enumerable
        IEnumerable<Member> GetContacts(string searchText = null)
        {

            if (string.IsNullOrEmpty(searchText))
            {
                return getMembers;
            }
            else
            {
                ObservableCollection<Member> members = new ObservableCollection<Member>(mymembers);

               return members.Where(p => p.FullName.ToLower().Contains(searchText) || p.FullName.ToUpper().Contains(searchText));
                
            }

        }
    }
}