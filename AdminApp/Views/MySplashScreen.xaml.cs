﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using AdminApp.ViewModels;
using AdminApp.Models;
using AdminApp.MyInterfaces.IServices;
using AdminApp.Repository;

using AdminApp.Container;
using AdminApp.MyInterfaces.IServices.General;
using AdminApp.Services;

namespace AdminApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MySplashScreen : ContentPage
    {
        public MySplashScreen()
        {
            InitializeComponent();
          
        }

        protected async override void OnAppearing()
        {
            await CheckLogin();
            base.OnAppearing();
        }

        private async Task CheckLogin()
        {
                var connectionService = new ConnectionService();
            try
            {
                if (connectionService.IsConnected)
                {
                    if (Settings.Email != string.Empty && Settings.Password != string.Empty)
                    {
                        if (Settings.Email == "username" && Settings.Password == "password")
                        {
                            App.Current.MainPage = new NavigationPage(new Login());
                        }
                        else
                        {

                            //Redirect to you desired page
                            IAuthenticationService _authenticationService = new ApiServices(new GenericRepository());
                            AuthenticationResponse authenticationResponse = new AuthenticationResponse();
                            authenticationResponse = await _authenticationService.Authenticate(Settings.Email, Settings.Password);
                            if (authenticationResponse == null)
                            {

                                Application.Current.MainPage = new NavigationPage(new Login());

                            }
                            else
                            {

                                string authentication = authenticationResponse.Token;
                                var Userrole = authenticationResponse.UserRoles;

                                if (!(String.IsNullOrEmpty(authentication)))
                                {

                                    App.Current.MainPage = new NavigationPage(new MainPage(authentication, Userrole, Settings.Bank));

                                }
                                else
                                {
                                    App.Current.MainPage = new NavigationPage(new Login());
                                }
                            }

                        }

                    }
                    else
                    {
                        //Redirect to login page.
                        App.Current.MainPage = new NavigationPage(new Login());
                    }
                }
                else
                {
                    await DisplayAlert("Check your connection", "No Network Connection Detected ", "OK");
                    await CheckLogin();
                }
            }
            catch
            {
               await CheckLogin();
            }
              
        }
    }
}