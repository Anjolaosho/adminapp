﻿using Rg.Plugins.Popup.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AdminApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    // Image popup page
    public partial class Page1 : Rg.Plugins.Popup.Pages.PopupPage
    {
        public Page1(Uri memberimage,string token)
        {
            InitializeComponent();

            memberimg.Source = memberimage;
        }

        protected override bool OnBackButtonPressed()
        {
            Navigation.PopAllPopupAsync();
            return true;
        }
    }
}