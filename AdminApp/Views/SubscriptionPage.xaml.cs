﻿using AdminApp.Container;
using AdminApp.Models;
using AdminApp.Models.PaymentsModel;
using AdminApp.MyInterfaces.IServices.Data;
using AdminApp.MyInterfaces.IServices.General;
using AdminApp.Repository;
using AdminApp.Services;
using AdminApp.Services.Data;
using AdminApp.ViewModels.PaymentsViewModel;
using PCLStorage;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AdminApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SubscriptionPage : ContentPage
    {
        private readonly IConnectionService connectionService;
        private readonly IPayments payments;

        private string token;
        private List<MainSubscription> mysubscriptions = new List<MainSubscription>();

        private ObservableCollection<MainSubscription> getSubscriptions;
        public ObservableCollection<MainSubscription> GetSubscriptions { get; set; }

        public SubscriptionPage(string authenticationResponse)
        {
            InitializeComponent();
            connectionService = new ConnectionService();
            payments = new PaymentServices(new GenericRepository());
            token = authenticationResponse;

            BindingContext = new SubscriptionViewModel(connectionService, this.Navigation, payments, authenticationResponse);
        }

        private void ListView_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            ((ListView)sender).SelectedItem = null;
        }

        public async Task<List<MainSubscription>> InitializeAsync()
        {
            IsBusy = true;
            try
            {
                var getevents = await payments.GetSubscriptions(token);

                IsBusy = false;
                //   myevents = getevents;
                return getevents;
            }
            catch
            {
                var getevents = await payments.GetSubscriptions(token);

                IsBusy = false;
                //   myevents = getevents;
                return getevents;
            }
           
        }

        protected async override void OnAppearing()
        {
            mysubscriptions = await InitializeAsync();
            getSubscriptions = new ObservableCollection<MainSubscription>(mysubscriptions);
            getsubs.ItemsSource = getSubscriptions;
            getimage();
        }

        private async void getimage()
        {
            IFolder rootFolder = FileSystem.Current.LocalStorage;

            var myfile = await rootFolder.GetFolderAsync("Pipu");
            if (await PCLHelper.IsFileExistAsync("Logo", myfile))
            {
                // await myfile.GetFileAsync("Logo");
                var theimage = await PCLHelper.LoadImage(new byte[0], "Logo", myfile); //new byte[96],
                var img = Encoding.ASCII.GetString(theimage);
                UserImage.Source = img;
            }

            if (await PCLHelper.IsFileExistAsync("AuthFile", myfile))
            {
                orgname.Text = await PCLHelper.ReadAllTextAsync("AuthFile", myfile);
            }

        }

        private async void TapGestureRecognizer_Tapped(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new SuscribeMember(token));
        }

        private Boolean blnShouldStay = false;
        protected override bool OnBackButtonPressed()
        {
            Task.Delay(2000).ContinueWith((args) =>
            {
                Device.BeginInvokeOnMainThread(async () =>
                {
                    var exit = await DisplayAlert("Exit Now", "Are you sure you want to Exit the app ?", "Yes", "No");
                    if (exit)
                    {
                        blnShouldStay = false;
                    }
                    else
                    {
                        blnShouldStay = true;
                    }
                });
            });
            if (blnShouldStay)
            {
                // Yes, we want to stay.
                return true;
            }
            else
            {
                // It's okay, we can leave.
                base.OnBackButtonPressed();
                return false;
            }

        }
    }
}