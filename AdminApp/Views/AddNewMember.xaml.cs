﻿using AdminApp.Container;
using AdminApp.Models;
using AdminApp.MyInterfaces.IRepository;
using AdminApp.MyInterfaces.IServices.Data;
using AdminApp.MyInterfaces.IServices.General;
using AdminApp.Repository;
using AdminApp.Services;
using AdminApp.Services.Data;
using AdminApp.ViewModels.MembersViewModel;
using PCLStorage;
using Plugin.Media;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AdminApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AddNewMember : ContentPage
    {
        private readonly IConnectionService connectionService;
        private readonly IMembers members;
      //  private readonly IGenericRepository genericRepository;

        public AddNewMember(string authenticationResponse)
        {
            InitializeComponent();

          //  genericRepository = new GenericRepository();
            connectionService = new ConnectionService();
            members = new MemberServices(new GenericRepository());

            BindingContext = new AddMembersViewModel(connectionService,this.Navigation,members,authenticationResponse);
        }

        private void TapGestureRecognizer_Tapped(object sender, EventArgs e)
        {
            ((AddMembersViewModel)BindingContext).Takepicture();
        
        }

        protected override void OnAppearing()
        {
            getimage();
            base.OnAppearing();
        }

        private async void getimage()
        {
            IFolder rootFolder = FileSystem.Current.LocalStorage;

            var myfile = await rootFolder.GetFolderAsync("Pipu");
            //if (await PCLHelper.IsFileExistAsync("Logo", myfile))
            //{
            //    // await myfile.GetFileAsync("Logo");
            //    var theimage = await PCLHelper.LoadImage(new byte[0], "Logo", myfile); //new byte[96],
            //    var img = Encoding.ASCII.GetString(theimage);
            //    UserImage.Source = img;
            //}

            if (await PCLHelper.IsFileExistAsync("AuthFile", myfile))
            {
                orgname.Text = await PCLHelper.ReadAllTextAsync("AuthFile", myfile);
            }

        }
    }
}