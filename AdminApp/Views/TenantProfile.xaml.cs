﻿using AdminApp.constants;
using AdminApp.Container;
using AdminApp.Models;
using AdminApp.MyInterfaces.IRepository;
using AdminApp.MyInterfaces.IServices;
using AdminApp.MyInterfaces.IServices.Data;
using AdminApp.MyInterfaces.IServices.General;
using AdminApp.Repository;
using AdminApp.Services;
using AdminApp.Services.Data;
using AdminApp.ViewModels.TenantViewModel;
using PCLStorage;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AdminApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TenantProfile : ContentPage
    {
        private readonly IAuthenticationService _authenticationService;
        private readonly IGenericRepository _genericRepository;
        private readonly IConnectionService connectionService;
        private readonly ITenant _tenant;
        private readonly IBanks Banks;
        private string token;
        private List<string> banks;
        private ObservableCollection<BankData> getallbanks;

        private List<BankData> allbanks = new List<BankData>();
        private List<string> allBank = new List<string>();

        public TenantProfile(string authenticationResponse)
        {
            InitializeComponent();
            token = authenticationResponse;
            connectionService = new ConnectionService();
            Banks = new BankdataServices(new GenericRepository());
            _genericRepository = new GenericRepository();
            _authenticationService = new ApiServices(_genericRepository);
            _tenant = new TenantServices(new GenericRepository());
            BindingContext = new TenntProfileViewModel(_genericRepository, connectionService, this.Navigation, _tenant , authenticationResponse);


        }

        private async Task<List<BankData>> GetAllbanks()     
        {
            var details = await Banks.banks(token);

            return details;
        }

        protected async override void OnAppearing()
        {
            
            allbanks = await GetAllbanks();
            //  getallbanks = new ObservableCollection<BankData>(allbanks);
            gettennant();
            allBank = allbanks.Select(boss => boss.Name).ToList();

            banklist.ItemsSource= allBank;

            getimage();
        }
        private async void getimage()
        {
            IFolder rootFolder = FileSystem.Current.LocalStorage;

            var myfile = await rootFolder.GetFolderAsync("Pipu");

            if (await PCLHelper.IsFileExistAsync("AuthFile", myfile))
            {
                orgnam.Text = await PCLHelper.ReadAllTextAsync("AuthFile", myfile);
            }

        }

      

        public async void gettennant()
        {
            try
            {
                var tenant = await _authenticationService.gettenant(token);

                var b = tenant.DateEstablished.ToString();


                Name.Text = tenant.NameOfOrganization;
                Email.Text = tenant.Email;
                ACno.Text = tenant.AccountNumber;
                address.Text = tenant.Address;
            }
            catch
            {
                var tenant = await _authenticationService.gettenant(token);

                var b = tenant.DateEstablished.ToString();


                Name.Text = tenant.NameOfOrganization;
                Email.Text = tenant.Email;
                ACno.Text = tenant.AccountNumber;
                address.Text = tenant.Address;
            }
           
           
        }

    }
}