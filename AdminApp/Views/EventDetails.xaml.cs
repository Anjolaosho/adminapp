﻿using AdminApp.Models.EventsModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AdminApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class EventDetails : ContentPage
    {
        public EventDetails(string token, Events events)
        {
            InitializeComponent();
            BindingContext = events;
        }
    }
}