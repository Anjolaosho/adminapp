﻿using AdminApp.Container;
using AdminApp.Models;
using AdminApp.Models.PaymentsModel;
using AdminApp.MyInterfaces.IServices.Data;
using AdminApp.MyInterfaces.IServices.General;
using AdminApp.Repository;
using AdminApp.Services;
using AdminApp.Services.Data;
using AdminApp.ViewModels.PaymentsViewModel;
using PCLStorage;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AdminApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AllPaymentsTempelate : ContentPage
    {
        private readonly IConnectionService connectionService;
        private readonly IPayments payments;

        private List<Payment> mypaymentplans = new List<Payment>();

        private List<Payment> allpaymentplans = new List<Payment>();
        private string token;
        private ObservableCollection<Payment> getpaymentplans;
        public AllPaymentsTempelate(string authenticationResponse)
        {
            InitializeComponent();

            connectionService = new ConnectionService();
            payments = new PaymentServices(new GenericRepository());
            token = authenticationResponse;
            BindingContext = new AllPaymentsViewModel(connectionService, this.Navigation, payments, authenticationResponse);
        }
        private async void TapGestureRecognizer_Tapped(object sender, EventArgs e)
        {
            var args = (TappedEventArgs)e;
            var myObject = args.Parameter;
            var item = (Payment)myObject;
            var delete = await DisplayAlert("Delete event", "Delete this Payment?", "Yes", "No");

            if (connectionService.IsConnected)
            {
                if (delete)
                {
                    await payments.DeletePayment(item, token);
                    await DisplayAlert("Delete Payment", "Payment Deleted", "Ok");
                    getpaymentplans.Remove(item);
                }
            }
            else
            {
                await DisplayAlert("please check connection", "no connection detected", "Yes");
            }
        }

        public async Task<List<Payment>> InitializeAsync()
        {
            IsBusy = true;

            try
            {
                var getpayments = await payments.GetAllpaymentPlans(token);


                IsBusy = false;
                //   myevents = getevents;
                return getpayments;
            }
            catch
            {
                var getpayments = await payments.GetAllpaymentPlans(token);


                IsBusy = false;
                //   myevents = getevents;
                return getpayments;
            }
         
        }

        protected async override void OnAppearing()
        {
            mypaymentplans = await InitializeAsync();
            
            //using (SQLiteConnection conn = new SQLiteConnection(App.Filepath))
            //{

            //    if (IsTableExists("MyPayment"))
            //    {
            //        conn.CreateTable<Payment>();
            //        conn.Insert(mypaymentplans);
            //    }
            //    else
            //    {
            //         conn.CreateTable<Payment>();
            //        conn.Update(mypaymentplans);

            //    }
            //    allpaymentplans = conn.Table<Payment>().ToList();
            //}


            getpaymentplans = new ObservableCollection<Payment>(mypaymentplans);
            allpayments.ItemsSource = getpaymentplans;
            getimage();
        }

        //public bool IsTableExists(string tableName)
        //{
        //    try
        //    {
        //        using (SQLiteConnection conn = new SQLiteConnection(App.Filepath))
        //        {
        //            var tableInfo = conn.GetTableInfo(tableName);
        //            if (tableInfo.Count == 0)
        //            {
        //                return true;
        //            }
        //            else
        //            {
        //                return false;
        //            }
        //        }
        //    }
        //    catch
        //    {
        //        return false;
        //    }
       // }

        private void SearchBar_TextChanged(object sender, TextChangedEventArgs e)
        {
            allpayments.ItemsSource = GetContacts(e.NewTextValue);
        }

        IEnumerable<Payment> GetContacts(string searchText = null)
        {

            if (string.IsNullOrEmpty(searchText))
            {
                return getpaymentplans;
            }
            else
            {
                ObservableCollection<Payment> pay = new ObservableCollection<Payment>(mypaymentplans);
                return pay.Where(p => p.Name.ToLower().Contains(searchText) || p.Name.ToUpper().Contains(searchText));
                
            }

        }

        private void allpayments_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            Payment item = (Payment)e.Item;
            ((ListView)sender).SelectedItem = null;
          //  await Navigation.PushAsync(new PaymentPlanDetail(token,item));
           
        }

        private async void getimage()
        {
            IFolder rootFolder = FileSystem.Current.LocalStorage;

            var myfile = await rootFolder.GetFolderAsync("Pipu");
            if (await PCLHelper.IsFileExistAsync("Logo", myfile))
            {
                // await myfile.GetFileAsync("Logo");
                var theimage = await PCLHelper.LoadImage(new byte[0], "Logo", myfile); //new byte[96],
                var img = Encoding.ASCII.GetString(theimage);
                UserImage.Source = img;
            }

            if (await PCLHelper.IsFileExistAsync("AuthFile", myfile))
            {
                orgname.Text = await PCLHelper.ReadAllTextAsync("AuthFile", myfile);
            }

        }
    }
}