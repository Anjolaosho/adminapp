﻿using AdminApp.Container;
using AdminApp.MyInterfaces.IServices.Data;
using AdminApp.MyInterfaces.IServices.General;
using AdminApp.Repository;
using AdminApp.Services;
using AdminApp.Services.Data;
using AdminApp.ViewModels.TenantViewModel;
using PCLStorage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AdminApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TenantUser : ContentPage
    {
        private string token;

        private readonly IConnectionService connectionService;
        private readonly IUsertenant userTenant;

        private List<Models.TenantUser> myadmins = new List<Models.TenantUser>();

        public TenantUser(string Token)
        {
            InitializeComponent();
            connectionService = new ConnectionService();
            userTenant = new TenantUserServices(new GenericRepository());
            token = Token;
            BindingContext = new UserTenantViewModel(connectionService, this.Navigation, userTenant, token);
        }

        protected async override void OnAppearing()
        {
            myadmins = await InitializeAsync();
            getimage();
            getadmins.ItemsSource = myadmins;

        }

        public async Task<List<Models.TenantUser>> InitializeAsync()
        {
            IsBusy = true;

            var getallusers = await userTenant.GetTenantUsers(token);

            IsBusy = false;
            //   myevents = getevents;
            return getallusers;
        }

        private async void TapGestureRecognizer_Tapped(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new AddTenantUser(token));
        }

        private void ListView_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            ((ListView)sender).SelectedItem = null;
        }


        // Get image and name from the file i put them using pcl helpers
        private async void getimage()
        {
            IFolder rootFolder = FileSystem.Current.LocalStorage;

            // get the folder where the file is in 
            var myfile = await rootFolder.GetFolderAsync("Pipu");

            //if the file i want exists 
            if (await PCLHelper.IsFileExistAsync("Logo", myfile))
            {
                // await myfile.GetFileAsync("Logo");
                // Get the image inside the file 
                var theimage = await PCLHelper.LoadImage(new byte[0], "Logo", myfile); //new byte[96],
                var img = Encoding.ASCII.GetString(theimage);
                // the xamarin forms image source will be the image gotten from the file
                UserImage.Source = img;
            }

            //check if another file exists in the folder
            if (await PCLHelper.IsFileExistAsync("AuthFile", myfile))
            {
                //read the text inside the file and make orgname the text
                orgname.Text = await PCLHelper.ReadAllTextAsync("AuthFile", myfile);
            }

        }
    }
}