﻿using AdminApp.Container;
using AdminApp.Models.PaymentsModel;
using AdminApp.MyInterfaces.IServices.Data;
using AdminApp.MyInterfaces.IServices.General;
using AdminApp.Repository;
using AdminApp.Services;
using AdminApp.Services.Data;
using AdminApp.ViewModels.MembersViewModel;
using PCLStorage;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AdminApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ActiveMembers : ContentPage
    {
        private readonly IConnectionService connectionService;
        private readonly IPayments payments;

        private string token;
        private List<MainSubscription> myActivesub = new List<MainSubscription>();

        private ObservableCollection<MainSubscription> getActiveSubscriptions;
        public ObservableCollection<MainSubscription> GetSubscriptions { get; set; }

        public ActiveMembers(string authenticationResponse)
        {
            InitializeComponent();
            connectionService = new ConnectionService();
            payments = new PaymentServices(new GenericRepository());
            token = authenticationResponse;

            BindingContext = new ActiveViewModel(connectionService, this.Navigation, payments, authenticationResponse);
        }

        private void ListView_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            ((ListView)sender).SelectedItem = null;
        }

        public async Task<List<MainSubscription>> InitializeAsync()
        {
            IsBusy = true;
            try
            {
                var GetActivesub = await payments.GetActivesubscriptions(token);

                IsBusy = false;
                //   myevents = getevents;
                return GetActivesub;
            }
            catch
            {
                var GetActivesub = await payments.GetActivesubscriptions(token);

                IsBusy = false;
                //   myevents = getevents;
                return GetActivesub;
            }
           
        }

        protected async override void OnAppearing()
        {
            myActivesub = await InitializeAsync();
            getActiveSubscriptions = new ObservableCollection<MainSubscription>(myActivesub);
            getsubs.ItemsSource = getActiveSubscriptions;
            getimage();
        }

        private async void getimage()
        {
            IFolder rootFolder = FileSystem.Current.LocalStorage;

            var myfile = await rootFolder.GetFolderAsync("Pipu");
            if (await PCLHelper.IsFileExistAsync("Logo", myfile))
            {
                // await myfile.GetFileAsync("Logo");
                var theimage = await PCLHelper.LoadImage(new byte[0], "Logo", myfile); //new byte[96],
                var img = Encoding.ASCII.GetString(theimage);
                UserImage.Source = img;
            }

            if (await PCLHelper.IsFileExistAsync("AuthFile", myfile))
            {
                orgname.Text = await PCLHelper.ReadAllTextAsync("AuthFile", myfile);
            }

        }
    }
}