﻿using AdminApp.Container;
using AdminApp.Models;
using AdminApp.ViewModels;
using PCLStorage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AdminApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Home : ContentPage
    {
        private string Token;
        public Home(string authenticationResponse)
        {
            InitializeComponent();

            Token = authenticationResponse;
            BindingContext = new HomeViewModel(authenticationResponse, this.Navigation);
        }

        private async void MembersTapGestureRecognizer_Tapped(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new AllMembersTempelate(Token));
        }

        private async void PaymentssTapGestureRecognizer_Tapped(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new AllPaymentsTempelate(Token));
        }

        private async void EventsTapGestureRecognizer_Tapped(object sender, EventArgs e)
        {
             await Navigation.PushAsync(new AllEventsTempelates(Token));
        }

        private async void NoticesTapGestureRecognizer_Tapped(object sender, EventArgs e)
        {
             await Navigation.PushAsync(new AllNoticesTempelate(Token));
        }

        private async void Button_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new AddNewMember(Token));
        }

        private async void Button_Clicked_1(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new AddNewEvent(Token));
        }

        private async void Button_Clicked_2(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new AddNewNotice(Token));
        }

        private async void Button_Clicked_3(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new AddPaymentPlan(Token));
        }

        protected override void OnAppearing()
        {
            getimage();
            base.OnAppearing();
        }

        private async void getimage()
        {
            IFolder rootFolder = FileSystem.Current.LocalStorage;

            var myfile = await rootFolder.GetFolderAsync("Pipu");
            if (await PCLHelper.IsFileExistAsync("Logo", myfile))
            {
                // await myfile.GetFileAsync("Logo");
                var theimage = await PCLHelper.LoadImage(new byte[0], "Logo", myfile); //new byte[96],
                var img = Encoding.ASCII.GetString(theimage);
                UserImage.Source = img;
            }

            if (await PCLHelper.IsFileExistAsync("AuthFile", myfile))
            {
                orgname.Text = await PCLHelper.ReadAllTextAsync("AuthFile", myfile);
            }

        }
        private Boolean blnShouldStay = false;
        protected override bool OnBackButtonPressed()
        {
            Task.Delay(2000).ContinueWith((args) =>
            {
                Device.BeginInvokeOnMainThread(async () =>
                {
                    var exit = await DisplayAlert("Exit Now", "Are you sure you want to Exit the app ?", "Yes", "No");
                    if (exit)
                    {
                        blnShouldStay = false;
                    }
                    else
                    {
                        blnShouldStay = true;
                    }
                });
            });
            if (blnShouldStay)
            {
                // Yes, we want to stay.
                return true;
            }
            else
            {
                // It's okay, we can leave.
                base.OnBackButtonPressed();
                return false;
            }

        }

    }
}