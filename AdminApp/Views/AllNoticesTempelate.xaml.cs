﻿using AdminApp.Container;
using AdminApp.Models;
using AdminApp.Models.NoticeModel;
using AdminApp.MyInterfaces.IServices.Data;
using AdminApp.MyInterfaces.IServices.General;
using AdminApp.Repository;
using AdminApp.Services;
using AdminApp.Services.Data;
using AdminApp.ViewModels.NoticesViewModel;
using PCLStorage;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AdminApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AllNoticesTempelate : ContentPage
    {

        private readonly IConnectionService connectionService;
        private readonly INotices Notices;
        private string token;

        private List<YourNotice> mynotices = new List<YourNotice>();

        private ObservableCollection<YourNotice> getNotice;
        public ObservableCollection<YourNotice> GetNotice { get; set; }
        public AllNoticesTempelate(string authenticationResponse)
        {
            InitializeComponent();
            connectionService = new ConnectionService();
            Notices = new NoticesServices(new GenericRepository());
            token = authenticationResponse;
            BindingContext = new AllNoticesViewModel(connectionService, this.Navigation, Notices, authenticationResponse);

        }

        private async void TapGestureRecognizer_Tapped(object sender, EventArgs e)
        {
            var args = (TappedEventArgs)e;
            var myObject = args.Parameter;
            var item = (YourNotice)myObject;
            var delete = await DisplayAlert("Delete event", "Delete this Notice?", "Yes", "No");

            if (connectionService.IsConnected)
            {
                if (delete)
                {
                    await Notices.DeleteEvent(item, token);
                    await DisplayAlert("Delete Notice", "Notice Deleted", "Ok");
                    getNotice.Remove(item);
                }
            }
            else
            {
                await DisplayAlert("please check connection", "no connection detected", "Yes");
            }
        }

        public async Task<List<YourNotice>> InitializeAsync()
        {
            IsBusy = true;
            try
            {
                var getnotices = await Notices.GetAllNoticeAsync(token);

                IsBusy = false;
                //   myevents = getevents;
                return getnotices;
            }
            catch
            {
                var getnotices = await Notices.GetAllNoticeAsync(token);

                IsBusy = false;
                //   myevents = getevents;
                return getnotices;
            }
           
        }

        protected async override void OnAppearing()
        {
            mynotices = await InitializeAsync();
            getNotice = new ObservableCollection<YourNotice>(mynotices);
            noticelist.ItemsSource = getNotice;
            getimage();
        }

        private async void noticelist_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            YourNotice yourNotice = (YourNotice)e.Item;
            ((ListView)sender).SelectedItem = null;
            await Navigation.PushAsync(new NoticeDetail(token, yourNotice));
        }

        private async void getimage()
        {
            IFolder rootFolder = FileSystem.Current.LocalStorage;

            var myfile = await rootFolder.GetFolderAsync("Pipu");
            if (await PCLHelper.IsFileExistAsync("Logo", myfile))
            {
                // await myfile.GetFileAsync("Logo");
                var theimage = await PCLHelper.LoadImage(new byte[0], "Logo", myfile); //new byte[96],
                var img = Encoding.ASCII.GetString(theimage);
                UserImage.Source = img;
            }

            if (await PCLHelper.IsFileExistAsync("AuthFile", myfile))
            {
                orgname.Text = await PCLHelper.ReadAllTextAsync("AuthFile", myfile);
            }

        }

        private void SearchBar_TextChanged(object sender, TextChangedEventArgs e)
        {
            noticelist.ItemsSource = GetContacts(e.NewTextValue);
        }

        IEnumerable<YourNotice> GetContacts(string searchText = null)
        {

            if (string.IsNullOrEmpty(searchText))
            {
                return getNotice;
            }
            else
            {
                ObservableCollection<YourNotice> pay = new ObservableCollection<YourNotice>(mynotices);
                return pay.Where(p => p.Subject.ToLower().Contains(searchText) || p.Subject.ToUpper().Contains(searchText));
            }

        }
    }
}