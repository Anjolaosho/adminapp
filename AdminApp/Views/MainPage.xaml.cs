﻿using AdminApp.Models;
using System;
using System.ComponentModel;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AdminApp.Views
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : TabbedPage
    {
        public SubscriptionPage subscriptionPage { set; get; }
        public Home home { set; get; }

        public AuthenticationResponse _GetAuthenticationResponse { get; set; }
        public MainPage(string authenticationResponse= "", string userrole = "" , string bank = null)                    
        {
            InitializeComponent();

            if(userrole == "Superadmin")
            {
                var tenantView = new NavigationPage(new TenantView(authenticationResponse,bank));
                tenantView.IconImageSource = "teamleader";
                tenantView.Title = "SuperAdmin";     

                var home = new NavigationPage(new Home(authenticationResponse));
                home.IconImageSource = "Homeicon.png";
                home.Title = "Home";

                var subscriptionPage = new NavigationPage(new SubscriptionPage(authenticationResponse));
                subscriptionPage.IconImageSource = "Subscriptionicon.png";
                subscriptionPage.Title = "Payments";

                Children.Add(tenantView);
                Children.Add(home);
                Children.Add(subscriptionPage);
            }

              if(userrole == "Admin")
            {
                var home = new NavigationPage(new Home(authenticationResponse));
                home.IconImageSource = "Homeicon.png";
                home.Title = "Home";

                var subscriptionPage = new NavigationPage(new SubscriptionPage(authenticationResponse));
                subscriptionPage.IconImageSource = "Subscriptionicon.png";
                subscriptionPage.Title = "Subscription";

                Children.Add(home);
                Children.Add(subscriptionPage);
            }


        }

        private Boolean blnShouldStay = true;
        protected override bool OnBackButtonPressed()
        {

            if (blnShouldStay)
            {
                Showdisplayalert();
                return blnShouldStay;
            }
            else
            {
                //return blnShouldStay;
               return base.OnBackButtonPressed();
            }
           
        }

        private async void Showdisplayalert()
        {
         
                var exit = await DisplayAlert("Exit Now", "Are you sure you want to Exit the app ?", "Yes", "No");
                if (exit)
                {

                    blnShouldStay = false;
                    OnBackButtonPressed();
                }

        }
    }
}