﻿using AdminApp.constants;
using AdminApp.Models.MembersModel;
using AdminApp.MyInterfaces.IRepository;
using AdminApp.MyInterfaces.IServices.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AdminApp.Services.Data
{
    class MemberServices : IMembers
    {
        private readonly IGenericRepository _genericRepository;

        public MemberServices(IGenericRepository genericRepository)
        {
            _genericRepository = genericRepository;
        }

        public Task<AddMember> addMember(AddMember addMember, string authtoken = "")
        {
            UriBuilder uriBuilder = new UriBuilder(ApiConstants.BaseApiUr)
            {
                Path = ApiConstants.NewMember,
                
            };

            return _genericRepository.PostAsync<AddMember>(uriBuilder.ToString(), addMember, authtoken);
        }

        public Task DeleteMember(Member member, string authtoken = "")
        {
            throw new NotImplementedException();
        }

        public async Task<List<Member>> GetAllMembersAsync(string authtoken = "")
        {
            UriBuilder builder = new UriBuilder(ApiConstants.BaseApiUr)
            {
                Path = ApiConstants.AllMembersEndpoint,
            };

            var allmembers = await _genericRepository.GetAsync<List<Member>>(builder.ToString(), authtoken);

            //   await Cache.InsertObject(CacheNameConstants.AllPies, pies, DateTimeOffset.Now.AddSeconds(20));

            return allmembers;
        }
    }
}
