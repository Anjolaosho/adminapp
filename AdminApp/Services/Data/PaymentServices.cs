﻿using AdminApp.constants;
using AdminApp.Models.MembersModel;
using AdminApp.Models.PaymentsModel;
using AdminApp.MyInterfaces.IRepository;
using AdminApp.MyInterfaces.IServices.Data;
using Akavache;
using System;
using System.Collections.Generic;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdminApp.Services.Data
{
    public class PaymentServices : BaseService,IPayments
    {
        private readonly IGenericRepository _genericRepository;
        public PaymentServices(IGenericRepository genericRepository, IBlobCache cache = null) : base(cache)
        {
            _genericRepository = genericRepository;
        }

        public Task DeletePayment(Payment payment, string authtoken = "")
        {
            UriBuilder uriBuilder = new UriBuilder(ApiConstants.BaseApiUr)
            {
                Path = $"{ApiConstants.DeletePaymentStructure}/{payment.Id}",

            };

            return _genericRepository.PutAsync<Payment>(uriBuilder.ToString(), payment, authtoken);
        }

        public async Task<List<MainSubscription>> GetActivesubscriptions(string authtoken = "")
        {
            List<MainSubscription> Activesubscriptionsfromcach = await GetFromCache<List<MainSubscription>>(CacheNameConstants.AllActiveSub);

            if (Activesubscriptionsfromcach != null)//loaded from cache
            {
                return Activesubscriptionsfromcach;
            }
            else
            {
                UriBuilder builder = new UriBuilder(ApiConstants.BaseApiUr)
                {
                    Path = ApiConstants.SubscriptionsActive
                };

                var Activesubscriptions = await _genericRepository.GetAsync<List<MainSubscription>>(builder.ToString(), authtoken);

                await Cache.InsertObject(CacheNameConstants.AllActiveSub, Activesubscriptions, DateTimeOffset.Now.AddSeconds(20));

                return Activesubscriptions;
            }
           
        }

        public async Task<List<Payment>> GetAllpaymentPlans(string authtoken = "")
        {
            List<Payment> AllPaymentsplanscach = await GetFromCache<List<Payment>>(CacheNameConstants.AllPaymentsplans);

            if (AllPaymentsplanscach != null)//loaded from cache
            {
                return AllPaymentsplanscach;
            }
            else
            {
                UriBuilder builder = new UriBuilder(ApiConstants.BaseApiUr)
                {
                    Path = ApiConstants.AllPayment
                };

                var allpayment = await _genericRepository.GetAsync<List<Payment>>(builder.ToString(), authtoken);

                await Cache.InsertObject(CacheNameConstants.AllPaymentsplans, allpayment, DateTimeOffset.Now.AddDays(1));
                return allpayment;
            }
           
        }

        public async Task<List<MainSubscription>> GetExpiredsubscriptions(string authtoken = "")
        {

            List<MainSubscription> AllExpiredplanscach = await GetFromCache<List<MainSubscription>>(CacheNameConstants.AllExpiredSubs);

            if (AllExpiredplanscach != null)//loaded from cache
            {
                return AllExpiredplanscach;
            }
            else
            {
                UriBuilder builder = new UriBuilder(ApiConstants.BaseApiUr)
                {
                    Path = ApiConstants.SubscriptionsExpired
                };

                var Expiredsubscriptions = await _genericRepository.GetAsync<List<MainSubscription>>(builder.ToString(), authtoken);

                await Cache.InsertObject(CacheNameConstants.AllPaymentsplans, Expiredsubscriptions, DateTimeOffset.Now.AddSeconds(20));

                return Expiredsubscriptions;
            }
          
        }

        public async Task<List<PaymentRecords>> GetPaymentsAsync(Member member, PaymentRecords payment, string authtoken = "")
        {
            UriBuilder builder = new UriBuilder(ApiConstants.BaseApiUr)
            {
                Path = $"{ApiConstants.PaymentRecord}/{member.Id}",
            };

            var getPayments = await _genericRepository.GetAsync<List<PaymentRecords>>(builder.ToString(), authtoken);


            return getPayments;
        }

        public async Task<List<MainSubscription>> GetSubscriptions(string authtoken = "")
        {
            List<MainSubscription> AllSubscriptionscach = await GetFromCache<List<MainSubscription>>(CacheNameConstants.AllSubscriptions);

            if (AllSubscriptionscach != null)//loaded from cache
            {
                return AllSubscriptionscach;
            }
            else
            {
                UriBuilder builder = new UriBuilder(ApiConstants.BaseApiUr)
                {
                    Path = ApiConstants.Subscriptions
                };

                var getPayments = await _genericRepository.GetAsync<List<MainSubscription>>(builder.ToString(), authtoken);


                return getPayments;
            }
               
        }

        public Task<MainSub> MakePayment(MainSub main, string authtoken = "")
        {
            UriBuilder uriBuilder = new UriBuilder(ApiConstants.BaseApiUr)
            {
                Path = ApiConstants.AddSubscriptions

            };

            return _genericRepository.PostAsync<MainSub>(uriBuilder.ToString(), main, authtoken);
        }

        public Task<AddPayment> payments(AddPayment payment, string authtoken = "")
        {
            UriBuilder uriBuilder = new UriBuilder(ApiConstants.BaseApiUr)
            {
                Path = ApiConstants.AddPayments,

            };

            return _genericRepository.PostAsync<AddPayment>(uriBuilder.ToString(), payment, authtoken);
        }
    }
}
