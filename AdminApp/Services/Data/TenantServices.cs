﻿using AdminApp.constants;
using AdminApp.Models;
using AdminApp.MyInterfaces.IRepository;
using AdminApp.MyInterfaces.IServices.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace AdminApp.Services.Data
{
    class TenantServices : ITenant
    {
        private readonly IGenericRepository _genericRepository;

        public TenantServices(IGenericRepository genericRepository)
        {
            _genericRepository = genericRepository;
        }

        public async Task<UpdateTenant> updateTenant(DateTime dateEstablished , string nameOfOrganization, string email, string accountNumber =" ",
                string bank = "", string address = "", AzureImageObject azureImage = null,  string authtoken = "")
        {              
            UpdateTenant update = new UpdateTenant()
            {
                Name = nameOfOrganization,
                Email = email,
                AccountNumber = accountNumber,
                Bank = bank,
                LogoPath = azureImage.pictureName,
                LogoBlobUrl = azureImage.picturePath,
                DateEstablished = dateEstablished,
                Address = address,
            };

            UriBuilder uriBuilder = new UriBuilder(ApiConstants.BaseApiUr)
            {
                Path = ApiConstants.UpdateTenant,

            };

            return await _genericRepository.PutAsync<UpdateTenant>(uriBuilder.ToString(), update, authtoken);
        }
    }
}
