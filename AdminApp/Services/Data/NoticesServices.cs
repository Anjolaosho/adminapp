﻿using AdminApp.constants;
using AdminApp.Models.NoticeModel;
using AdminApp.MyInterfaces.IRepository;
using AdminApp.MyInterfaces.IServices.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AdminApp.Services.Data
{
    class NoticesServices : INotices
    {

        private readonly IGenericRepository _genericRepository;

        public NoticesServices(IGenericRepository genericRepository)
        {
            _genericRepository = genericRepository;
        }

        public Task DeleteEvent(YourNotice notices, string authtoken = "")
        {
            UriBuilder uriBuilder = new UriBuilder(ApiConstants.BaseApiUr)
            {
                Path = $"{ApiConstants.DeleteNotice}/{notices.Id}",

            };

            return _genericRepository.DeleteAsync(uriBuilder.ToString(), authtoken);
        }

        public async Task<List<YourNotice>> GetAllNoticeAsync(string authtoken = "")
        {
            UriBuilder builder = new UriBuilder(ApiConstants.BaseApiUr)
            {
                Path = ApiConstants.AllNotices
            };

            var allNotices = await _genericRepository.GetAsync<List<YourNotice>>(builder.ToString(), authtoken);

            //   await Cache.InsertObject(CacheNameConstants.AllPies, pies, DateTimeOffset.Now.AddSeconds(20));

            return allNotices;
        }

        public Task<Notices> notices(Notices notices, string authtoken = "")
        {
            UriBuilder uriBuilder = new UriBuilder(ApiConstants.BaseApiUr)
            {
                Path = ApiConstants.NewNotices

            };

            return _genericRepository.PostAsync<Notices>(uriBuilder.ToString(), notices, authtoken);
        }
    }
}
