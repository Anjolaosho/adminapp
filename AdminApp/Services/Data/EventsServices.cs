﻿using AdminApp.constants;
using AdminApp.Models.EventsModel;
using AdminApp.MyInterfaces.IRepository;
using AdminApp.MyInterfaces.IServices.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AdminApp.Services.Data
{
    class EventsServices : IEvents
    {
        private readonly IGenericRepository _genericRepository;

        public EventsServices(IGenericRepository genericRepository)
        {
            _genericRepository = genericRepository;
        }

        public Task DeleteEvent(Events @event, string authtoken = "")
        {
            UriBuilder uriBuilder = new UriBuilder(ApiConstants.BaseApiUr)
            {
                Path = $"{ApiConstants.DeleteEvent}/{@event.Id}",

            };

            return _genericRepository.DeleteAsync(uriBuilder.ToString(), authtoken);
        }

        public Task<Event> @event(Event @event, string authtoken = "")
        {
            UriBuilder uriBuilder = new UriBuilder(ApiConstants.BaseApiUr)
            {
                Path = ApiConstants.NewEvent

            };

            return _genericRepository.PostAsync<Event>(uriBuilder.ToString(), @event, authtoken);
        }

        public async Task<List<Events>> GetAllEventsAsync(string authtoken = "")
        {
            UriBuilder builder = new UriBuilder(ApiConstants.BaseApiUr)
            {
                Path = ApiConstants.AllEvent
            };

            var allevents = await _genericRepository.GetAsync<List<Events>>(builder.ToString(),authtoken);

         //   await Cache.InsertObject(CacheNameConstants.AllPies, pies, DateTimeOffset.Now.AddSeconds(20));

            return allevents;
        }
    }
}
