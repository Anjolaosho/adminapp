﻿using AdminApp.constants;
using AdminApp.Models;
using AdminApp.MyInterfaces.IRepository;
using AdminApp.MyInterfaces.IServices.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AdminApp.Services.Data
{
    class TenantUserServices : IUsertenant
    {
        private readonly IGenericRepository _genericRepository;

        public TenantUserServices(IGenericRepository genericRepository)
        {
            _genericRepository = genericRepository;
        }

        public Task<List<TenantUser>> GetTenantUsers(string authtoken)
        {
            UriBuilder uriBuilder = new UriBuilder(ApiConstants.BaseApiUr)
            {
                Path = ApiConstants.myusers,

            };

            return _genericRepository.GetAsync<List<TenantUser>>(uriBuilder.ToString(), authtoken);

        }

        public Task<TenantUser> tenantUser(string Name, string Email, string authtoken)
        {
            TenantUser addTenantUser = new TenantUser()
            {
                Name = Name,
                Email = Email,
            };

            UriBuilder uriBuilder = new UriBuilder(ApiConstants.BaseApiUr)
            {
                Path = ApiConstants.AddtenantUser

            };

            return _genericRepository.PostAsync<TenantUser>(uriBuilder.ToString(), addTenantUser, authtoken);

        }
    }
}
