﻿using AdminApp.constants;
using AdminApp.Models;
using AdminApp.MyInterfaces.IRepository;
using AdminApp.MyInterfaces.IServices.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdminApp.Services.Data
{
    class BankdataServices : IBanks
    {

        private readonly IGenericRepository _genericRepository;

        public BankdataServices(IGenericRepository genericRepository)
        {
            _genericRepository = genericRepository;
        }


        public async Task<List<BankData>> banks(string authtoken)
        {
            UriBuilder uriBuilder = new UriBuilder(ApiConstants.BaseApiUr)
            {
                Path = ApiConstants.Allbanks
            };

            var banklist = await _genericRepository.GetAsync<IEnumerable<BankData>>(uriBuilder.ToString(), authtoken);

            List<BankData> banks = banklist.ToList();

            return banks;
        }
    }
}
