﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using AdminApp.constants;
using AdminApp.Models;
using AdminApp.MyInterfaces.IRepository;
using AdminApp.MyInterfaces.IServices;
using Newtonsoft.Json;

namespace AdminApp.Services
{
    public class ApiServices : IAuthenticationService
    {
        
        private readonly IGenericRepository _genericRepository;

        public ApiServices(IGenericRepository genericRepository)
        {
            _genericRepository = genericRepository;

        }

        Task<AuthenticationResponse> IAuthenticationService.Register(string nameoforg, string email, string password)
        {
            // directing the request to its endpoint
            UriBuilder uriBuilder = new UriBuilder(ApiConstants.BaseApiUr)
            {
                Path = ApiConstants.RegisterEndpoint
            };
            AuthenticationRequest Request = new AuthenticationRequest()
            {
                Email = email,
                Password = password,
                NameOfOrganization = nameoforg,
            };
            return _genericRepository.PostAsync<AuthenticationRequest, AuthenticationResponse>(uriBuilder.ToString(), Request);
        }

        public Task<AuthenticationResponse> Authenticate(string email, string password)
        {
            UriBuilder uriBuilder = new UriBuilder(ApiConstants.BaseApiUr)
            {
                Path = ApiConstants.AuthenticateEndpoint
            };
            LoginDTO Request = new LoginDTO()
            {
                Username = email,
                Password = password,
            };

            return _genericRepository.PostAsync<LoginDTO, AuthenticationResponse>(uriBuilder.ToString(), Request);
        }

        public bool IsUserAuthenticated()
        {
            throw new NotImplementedException();
        }

        public Task<TenantProfile> gettenant(string authtoken = "")
        {
            UriBuilder uriBuilder = new UriBuilder(ApiConstants.BaseApiUr)
            {
                Path = ApiConstants.TenantProfile,
            };
        

            return _genericRepository.GetAsync<TenantProfile>(uriBuilder.ToString(), authtoken);
        }

        //checks if UserIdSetting is null and if it isnt it returns the UserIDSetting

    }
}
