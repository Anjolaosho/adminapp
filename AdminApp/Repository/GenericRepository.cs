﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using AdminApp.MyInterfaces.IRepository;
using System.Threading.Tasks;
using System.Net;
using System.Diagnostics;
using Polly;
using AdminApp.Exceptions;
using AdminApp.Models;
using Xamarin.Forms;

namespace AdminApp.Repository
{
    class GenericRepository : IGenericRepository
    {
        public async Task DeleteAsync(string uri, string authToken = "")
        {
            HttpClient httpClient = CreateHttpClient(authToken);

            await httpClient.DeleteAsync(uri);
        }

        //Implementation of the Get types interface
        public async Task<T> GetAsync<T>(string uri, string authToken = "")
        {
            try
            {
                HttpClient httpClient = CreateHttpClient(authToken);
                string jsonResult = string.Empty;


               // var responseMessage = await httpClient.GetAsync(uri);
                  var responseMessage = await Policy
                   .Handle<WebException>(ex =>
                  {
                        Debug.WriteLine($"{ex.GetType().Name + " : " + ex.Message}");
                        return true;
                    })
                    .WaitAndRetryAsync
                    (
                        7,
                        retryAttempt => TimeSpan.FromSeconds(Math.Pow(3, retryAttempt))
                    )
                     .ExecuteAsync(async () => await httpClient.GetAsync(uri));


                if (responseMessage.IsSuccessStatusCode)
                {
                    jsonResult = await responseMessage.Content.ReadAsStringAsync().ConfigureAwait(false);
                   
                    Type classtype = typeof(T);
                    if (classtype == typeof(IEnumerable<BankData>))
                    {
                        var jsonD = JsonConvert.DeserializeObject(jsonResult);
                        var json1 = JsonConvert.DeserializeObject<T>(jsonD.ToString());
                        return json1;
                    }

                    var json = JsonConvert.DeserializeObject<T>(jsonResult);
                    return json;
                }

                if (responseMessage.StatusCode == HttpStatusCode.Forbidden ||
                    responseMessage.StatusCode == HttpStatusCode.Unauthorized)
                {
                      throw new ServiceAuthenticationException(jsonResult);
                }

                  throw new HttpRequestExceptionEx(responseMessage.StatusCode, jsonResult);
               

            }
            catch (Exception e)
            {
                Debug.WriteLine($"{ e.GetType().Name + " : " + e.Message}");
                throw;
            }
        }
    

        public async Task<T> PostAsync<T>(string uri, T data, string authToken = "")
        {
            try
            {
                HttpClient httpClient = CreateHttpClient(authToken);


                   string jsonstring = JsonConvert.SerializeObject(data);

                    var content = new StringContent(jsonstring, Encoding.UTF8, "application/json");

             //   var content = new StringContent(jsonstring, Encoding.UTF8, "application/json");

                string jsonResult = string.Empty;

                var responseMessage = await Policy
                    .Handle<WebException>(ex =>
                    {
                        Debug.WriteLine($"{ex.GetType().Name + " : " + ex.Message}");
                        return true;
                    })
                    .WaitAndRetryAsync
                    (
                        5,
                        retryAttempt => TimeSpan.FromSeconds(Math.Pow(2, retryAttempt))
                    )
                    .ExecuteAsync(async () => await httpClient.PostAsync(uri, content));

                if (responseMessage.IsSuccessStatusCode)
                {
                    jsonResult = await responseMessage.Content.ReadAsStringAsync().ConfigureAwait(false);
                    var json = JsonConvert.DeserializeObject<T>(jsonResult);
                    return json;
                }

                if (responseMessage.StatusCode == HttpStatusCode.Forbidden ||
                    responseMessage.StatusCode == HttpStatusCode.Unauthorized)
                {
                     throw new ServiceAuthenticationException(jsonResult);
                }
                jsonResult = await responseMessage.Content.ReadAsStringAsync().ConfigureAwait(false);
                throw new HttpRequestExceptionEx(responseMessage.StatusCode, jsonResult);

            }
            catch (Exception e)
            {
                Debug.WriteLine($"{ e.GetType().Name + " : " + e.Message}");
                throw;
            }
        }



        //Implementation of the Post with two generic types interface
        public async Task<R> PostAsync<T, R>(string uri, T data, string authToken = "")    
        {
            try
            {
                HttpClient httpClient = CreateHttpClient(uri);

                string jsonstring = JsonConvert.SerializeObject(data);

                var content = new StringContent(jsonstring, Encoding.UTF8, "application/json");


                                                 //Multi
                //   content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                string jsonresult = string.Empty;

                var responseMessage = await Policy.Handle<WebException>(ex =>
               {
                   Debug.WriteLine($"{ex.GetType().Name + " : " + ex.Message}");
                   return true;
               }).WaitAndRetryAsync(5,
                retryAttempt => TimeSpan.FromSeconds(Math.Pow(2, retryAttempt)))
               .ExecuteAsync(async () => await httpClient.PostAsync(uri, content));

                Type classtype = typeof(T);
                if (classtype == typeof(LoginDTO))
                { 

                    if (responseMessage.IsSuccessStatusCode)
                    {
                        jsonresult = await responseMessage.Content.ReadAsStringAsync().ConfigureAwait(false);
                        var json = JsonConvert.DeserializeObject<R>(jsonresult);
                        return json;
                    }
                    else
                    {
                        jsonresult = await responseMessage.Content.ReadAsStringAsync().ConfigureAwait(false);
                        var json = JsonConvert.DeserializeObject<R>(jsonresult);
                        return json;
                    }
                }
                else
                {
                    if (responseMessage.IsSuccessStatusCode)
                    {
                        jsonresult = await responseMessage.Content.ReadAsStringAsync().ConfigureAwait(false);
                        var json = JsonConvert.DeserializeObject<R>(jsonresult);
                        return json;
                    }
                    else if(responseMessage.StatusCode == HttpStatusCode.Forbidden || responseMessage.StatusCode == HttpStatusCode.Unauthorized)
                    {

                        await Application.Current.MainPage.DisplayAlert($"Invalid attempt {jsonresult}", " email or UserName Invalid please change  ", "OK");
                        R obj = (R)Activator.CreateInstance(typeof(R));
                        return obj;
                    }
                    else
                    {
                        R obj = (R)Activator.CreateInstance(typeof(R));
                        return obj;
                    }
                   // throw new HttpRequestExceptionEx(responseMessage.StatusCode, jsonresult);
                    //if (responseMessage.StatusCode == HttpStatusCode.Forbidden || responseMessage.StatusCode == HttpStatusCode.Unauthorized)
                    //{
                    //    await Application.Current.MainPage.DisplayAlert($"Invalid attempt {jsonresult}", " email or UserName Invalid please change  ", "OK");
                    //}
                   
                }
                
            }

            catch (Exception e)
            {
                Debug.WriteLine($"{ e.GetType().Name + " : " + e.Message}");
                throw;
            }
        }

        public async Task<T> PutAsync<T>(string uri, T data, string authToken = "")
        {
            try
            {
                HttpClient httpClient = CreateHttpClient(authToken);

                var content = new StringContent(JsonConvert.SerializeObject(data));
                content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                string jsonResult = string.Empty;

                var responseMessage = await Policy
                    .Handle<WebException>(ex =>
                    {
                        Debug.WriteLine($"{ex.GetType().Name + " : " + ex.Message}");
                        return true;
                    })
                    .WaitAndRetryAsync
                    (
                        5,
                        retryAttempt => TimeSpan.FromSeconds(Math.Pow(2, retryAttempt))
                    )
                    .ExecuteAsync(async () => await httpClient.PutAsync(uri, content));

                if (responseMessage.IsSuccessStatusCode)
                {
                    jsonResult = await responseMessage.Content.ReadAsStringAsync().ConfigureAwait(false);
                    var json = JsonConvert.DeserializeObject<T>(jsonResult);
                    return json;
                }

                if (responseMessage.StatusCode == HttpStatusCode.Forbidden ||
                    responseMessage.StatusCode == HttpStatusCode.Unauthorized)
                {
                    throw new ServiceAuthenticationException(jsonResult);
                }

                throw new HttpRequestExceptionEx(responseMessage.StatusCode, jsonResult);

            }
            catch (Exception e)
            {
                Debug.WriteLine($"{ e.GetType().Name + " : " + e.Message}");
                throw;
            }
        }

    

    // Returns an httpclient object with a header and an authtoken inside it
    // Should only be accessed in this class because of the authtoken
    private HttpClient CreateHttpClient(string authtoken)
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            if (!string.IsNullOrEmpty(authtoken))
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", authtoken);  
            }
            return client;
        }
    }
}
