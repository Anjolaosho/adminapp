﻿using Plugin.Settings;
using Plugin.Settings.Abstractions;
using System;
using System.Collections.Generic;
using System.Text;

namespace AdminApp.Container
{
    public static class Settings
    {
        private static ISettings AppSettings
        {
            get
            {
                return CrossSettings.Current;
            }
        }

        //Setting Constants

        const string email = "username";
        private static readonly string EmailDefault = string.Empty;

        public static string Email
        {
            get { return AppSettings.GetValueOrDefault(email,EmailDefault); }
            set { AppSettings.AddOrUpdateValue(email, value); }
        }

        const string password = "password";
        private static readonly string PasswordDefault = string.Empty;

        public static string Password
        {
            get { return AppSettings.GetValueOrDefault(password, PasswordDefault); }
            set { AppSettings.AddOrUpdateValue(password, value); }
        }

        const string bank = "";
        private static readonly string BankDefault = string.Empty;

        public static string Bank
        {
            get { return AppSettings.GetValueOrDefault(bank, BankDefault); }
            set { AppSettings.AddOrUpdateValue(bank, value); }
        }
    }
}
