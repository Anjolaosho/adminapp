﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AdminApp.constants
{
    public class CacheNameConstants
    {
        public const string AllMembers = "AllMembers";
        public const string AllEvents = "AllEvents";
        public const string AllNotices = "AllNotices";
        public const string AllPaymentsplans = "AllPaymentplans";
        public const string AllActiveSub = "AllActiveSubs";
        public const string AllExpiredSubs = "AllExpiredsubs";
        public const string AllSubscriptions = "AllSubscriptions";

    }
}
