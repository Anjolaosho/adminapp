﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AdminApp.constants
{
    class ApiConstants
    {
        public const string BaseApiUr = "http://unionfaith.westeurope.cloudapp.azure.com:8080";
        public const string RegisterEndpoint = "/RegisterTenant";
        public const string AuthenticateEndpoint = "/Login";
        public const string AllMembersEndpoint = "/api/v1/members";
        public const string NewMember = "/api/v1/members/mobile-add";
        public const string NewNotices = "/api/v1/notices/add";
        public const string NewEvent = "/api/v1/events/add";
        public const string AllEvent = "/api/v1/events";
        public const string AllNotices = "/api/v1/notices";
        public const string AddPayments = "/api/v1/payments/add";
        public const string AllPayment = "/api/v1/payments";
        public const string PaymentRecord = "/api/v1/member/records";
        public const string Allbanks = "/api/v1/banklist";
        public const string UpdateTenant = "/api/v1/mobile-editprofile";
        public const string AddtenantUser = "/api/v1/myusers/add";
        public const string RecoverPassword = "/RecoverPassword";
        public const string ConfirmRecoverPassword = "/ConfirmRecoverPassword";
        public const string TenantProfile = "/api/v1/profile";
        public const string Subscriptions = "/api/v1/subscriptions";
        public const string AddSubscriptions = "/api/v1/subscriptions/add";
        public const string SubscriptionsActive = "/api/v1/subscriptions/active";
        public const string SubscriptionsExpired = "/api/v1/subscriptions/expired";
        public const string DeleteEvent = "/api/v1/events/delete";
        public const string DeleteNotice = "/api/v1/notices/delete";
        public const string DeletePaymentStructure = "/api/v1/payments/delete";
        public const string DeleteMemberStructure = "/api/v1/members/delete";
        public const string myusers = "/api/v1/myusers";
    }
}
