﻿using AdminApp.Models;
using AdminApp.Models.MembersModel;
using AdminApp.MyInterfaces.IServices.Data;
using AdminApp.MyInterfaces.IServices.General;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace AdminApp.ViewModels.MembersViewModel
{
    class AllMembersViewModel : BaseViewModel
    {
        private ObservableCollection<Member> allMembers;
        private string Token;
        private readonly IMembers _members;
        public INavigation Navigation { get; set; }
        protected readonly IConnectionService _connectionService;

        public AllMembersViewModel(IConnectionService connectionService, INavigation navigation,
            IMembers Members, string authenticationResponse)
        {
            
            _connectionService = connectionService;
            _members = Members;
            Navigation = navigation;
            Token = authenticationResponse;
            YourGreeting(DateTime.Now);
        }

        public ICommand Back => new Command(BackTapped);

        private void BackTapped()
        {
            Navigation.PopAsync();  
        }

        public ObservableCollection<Member> members
        {
            get => allMembers;
            set
            {
                allMembers = value;
                OnPropertyChanged();
            }
        }

        private string greeting;
        public string Greeting
        {
            get { return greeting; }
            set { SetProperty(ref greeting, value); }
        }

        public void YourGreeting(DateTime time)
        {

            if (time.Hour >= 0 && time.Hour < 12)
            {
                greeting = "Good Morning";
            }
            else if (time.Hour >= 12 && time.Hour < 18)
            {
                greeting = "Good Afternoon";
            }
            else if (time.Hour >= 18 && time.Hour <= 23)
            {
                greeting = "Good Evening";
            }

        }
    }
}
