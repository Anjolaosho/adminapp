﻿using AdminApp.Models;
using AdminApp.Models.MembersModel;
using AdminApp.MyInterfaces.IServices;
using AdminApp.MyInterfaces.IServices.Data;
using AdminApp.MyInterfaces.IServices.General;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Plugin.Media;
using Plugin.Media.Abstractions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace AdminApp.ViewModels.MembersViewModel
{
    public class AddMembersViewModel : BaseViewModel
    {

        Stream stream = null;

        MediaFile file;
        static string _storageConnection = "DefaultEndpointsProtocol=https;AccountName=churchplusstorage;AccountKey=rQZhE5UYZ9EdgzVZvr3bXLMNYEuoQG3jGW71uQFVeKxI+YR3iBlRyLWMxqOGTT83L3/6jRBH8uSSZkC3oJ+duA==;EndpointSuffix=core.windows.net";
        static CloudStorageAccount cloudStorageAccount = CloudStorageAccount.Parse(_storageConnection);
        static CloudBlobClient cloudBlobClient = cloudStorageAccount.CreateCloudBlobClient();
        static CloudBlobContainer cloudBlobContainer = cloudBlobClient.GetContainerReference("membershipmanager");
        AzureImageObject blobImageUploader;

        private readonly IMembers _members;
        protected readonly IConnectionService _connectionService;

        private Page page;
        private string Token;

        public INavigation _navigation { get; set; }

        private string _fullName;

        private string _profilePicturePath;

        private string _profilePictureBlobUrl;

        private string _emailAddress;

        private string _phoneNumber;

        private Gender gender;

        private string _residentialAddress;

        private string _birthday;

        private DateTime _dateJoined;

        private string wedding;


        public MultipartFormDataContent content { get; set; }
        public AddMembersViewModel(IConnectionService connectionService, INavigation Navigation,IMembers members, string authenticationResponse)
        {
            _connectionService = connectionService;
            _navigation = Navigation;
            _members = members;
            Token = authenticationResponse;
            blobImageUploader = new AzureImageObject();
            YourGreeting(DateTime.Now);
        }

        public string FullName
        { get { return _fullName; }
            set { SetProperty(ref _fullName, value); }
        }

        public string ProfilePicturePath
        { get { return _profilePicturePath; }
            set { SetProperty(ref _profilePicturePath, value); } }

        public string ProfilePictureBlobUrl
        { get { return _profilePictureBlobUrl; }
            set { SetProperty(ref _profilePictureBlobUrl, value); } }

        public string EmailAddress
        { get { return _emailAddress ; }
            set { SetProperty(ref _emailAddress, value); } }

        public string PhoneNumber
        { get { return _phoneNumber; }
            set { SetProperty(ref _phoneNumber, value); } }

        public Gender Gender
        {
            get { return gender; }
            set { SetProperty(ref gender, value); }
        }

        public string ResidentialAddress
        { get { return _residentialAddress; }
            set { SetProperty(ref _residentialAddress, value); } }

                                    
        public string Wedding { get { return wedding; } set { SetProperty(ref wedding, value); } }

        public DateTime DateJoined
        { get { return _dateJoined; }
            set { SetProperty(ref _dateJoined, value); } }

        public string Birthday
        { get { return _birthday; }
            set { SetProperty(ref _birthday, value); } }

        private Xamarin.Forms.Image _circleImage = new Xamarin.Forms.Image();
        public Xamarin.Forms.Image circleImage
        {
            get { return _circleImage; }
            set { SetProperty(ref _circleImage, value); }
        }        

        bool female; 
        bool male ;
        public bool Female
        {
            
            get { return female; }
            set {
                SetProperty(ref female, value);
                if (female == true)
                { 
                    Male = false; 
                    Gender = Gender.female; 
                }
            }
        }  public bool Male
        {
            
            get { return male; }
            set {
                SetProperty(ref male, value);
                if (male == true) { Female = false; Gender = Gender.Male; }
            }
        }
           public string Thegender()
        {
            if(gender == Gender.female)
            {
                return "Femail";
            }
            else
            {
                return "Male";
            }
        }

        public static string FletterToupper(string s)
        {
            if (string.IsNullOrEmpty(s))
            {
               Application.Current.MainPage.DisplayAlert("No Name Found", $"Please Input Name ", "OK");
                return string.Empty;
            }
            else
            {
                return char.ToUpper(s[0]) + s.Substring(1);
            }
        }

        public ICommand Back => new Command(BackTapped);

        private void BackTapped()
        {
            _navigation.PopAsync();
        }
        public ICommand sendcommand => new Command(Send);

        private async void Send()
        {
            try
            {
                if (_connectionService.IsConnected)
                {
                    if(_fullName != null && _phoneNumber != null)
                    {
                        if((PhoneNumber.Length == 11 && PhoneNumber.StartsWith("0")) || (PhoneNumber.Length >10 && PhoneNumber.StartsWith("+")))
                        {
                            if (_emailAddress.Contains("@") && _emailAddress.Contains("."))
                            {
                                if(_birthday != null && wedding != null && _dateJoined != null)
                                {
                                    IsBusy = true;
                                    await Application.Current.MainPage.DisplayAlert("Add member", $" Your member will be added shortely", "OK");

                                    if (file != null)
                                    {
                                        try
                                        {
                                            await UploadAsync();
                                            AddMember addMember = new AddMember()
                                            {
                                                FullName = FletterToupper(_fullName),
                                                EmailAddress = _emailAddress,
                                                PhoneNumber = _phoneNumber,
                                                ResidentialAddress = _residentialAddress,
                                                Birthday = date(_birthday),
                                                Wedding = date(wedding),
                                                DateJoined = _dateJoined,
                                                ProfilePicturePath = blobImageUploader.pictureName,
                                                ProfilePictureBlobUrl = blobImageUploader.picturePath,
                                                Gender = Thegender()
                                            };

                                            var memberadded = await _members.addMember(addMember, Token);
                                            IsBusy = false;
                                            await Application.Current.MainPage.DisplayAlert("Member Added", $"{addMember.FullName} has been added ", "OK");
                                            await _navigation.PopAsync();
                                        }
                                        catch
                                        {
                                            IsBusy = false;
                                            await Application.Current.MainPage.DisplayAlert("Invalid Input", $"Please Make sure all fields have been entered correctly and make sure email and Phonenumber has not ben used before ", "OK");
                                        }

                                    }
                                    else
                                    {
                                        var input = await Application.Current.MainPage.DisplayAlert("No Image", $"Are you sure you dont want to add an image ", "Yes", "No");

                                        if (input)
                                        {
                                            try
                                            {
                                                AddMember addMember = new AddMember()
                                                {
                                                    FullName = FletterToupper(_fullName),
                                                    Username = _phoneNumber,
                                                    EmailAddress = _emailAddress,
                                                    PhoneNumber = _phoneNumber,
                                                    ResidentialAddress = _residentialAddress,
                                                    Birthday = date(_birthday),
                                                    Wedding = date(wedding),
                                                    DateJoined = _dateJoined,
                                                    Gender = Thegender()
                                                };

                                                var memberadded = await _members.addMember(addMember, Token);
                                                IsBusy = true;
                                                await Application.Current.MainPage.DisplayAlert("Member Added", $"{addMember.FullName} has been added ", "OK");
                                                await _navigation.PopAsync();
                                            }
                                            catch
                                            {
                                                IsBusy = false;
                                                await Application.Current.MainPage.DisplayAlert("Invalid Input", $"Please Make sure all fields have been entered correctly and make sure email and Phonenumber has not ben used before ", "OK");
                                            }

                                        }

                                    }
                                }
                                else
                                {
                                    IsBusy = false;
                                    await Application.Current.MainPage.DisplayAlert("Invalid Input", $" date ", "OK");
                                }
                              
                            }
                            else
                            {
                                await Application.Current.MainPage.DisplayAlert("Invalid EmailAddress", $"Please enter a valid EmailAddress ", "OK");
                            }

                        }
                        else
                        {
                            await Application.Current.MainPage.DisplayAlert("Invalid Phonenumber", $"Please enter a valid Phonenumber ", "OK");
                        }

                    }
                    else
                    {
                        await Application.Current.MainPage.DisplayAlert("Important text empty", $"Please Enter your Phonenumber and fullName ", "OK");
                    }

                }
                else
                {
                    IsBusy = false;
                    await Application.Current.MainPage.DisplayAlert("Check your connection", "Please Check Network Connection ", "OK");
                }

            }
            catch
            {
             
            }
            
        }

        public async void Takepicture()
        {
            page = new Page();
            

            await CrossMedia.Current.Initialize();

            var pick = await Application.Current.MainPage.DisplayAlert("Get Picture", $"Do you want to Pick photo from Gallery or take photo with camera ", "Use Camera", "Use Gallery");

            if (pick)
            {
                if (!CrossMedia.Current.IsCameraAvailable ||
        !CrossMedia.Current.IsTakePhotoSupported)
                {
                    await Application.Current.MainPage.DisplayAlert("No Camera", "No camera available.", "OK");
                    return;
                }
                file = await CrossMedia.Current.TakePhotoAsync(
               new Plugin.Media.Abstractions.StoreCameraMediaOptions
               {
                   Directory = "Sample",

                   Name = "test.jpg"
               });
                if (file == null)
                    return;


                //  await Application.Current.MainPage.DisplayAlert("File Location", file.Path, "OK");

                _circleImage.Source = ImageSource.FromStream(() =>
                {
                    var stream = file.GetStream();
                    //  file.Dispose();
                    return stream;
                });
            }
            else
            {
                if (!CrossMedia.Current.IsPickPhotoSupported)
                {
                    await Application.Current.MainPage.DisplayAlert("Photos Not Supported", ":( Permission not granted to photos.", "OK");
                    return;
                }
                file = await Plugin.Media.CrossMedia.Current.PickPhotoAsync(new Plugin.Media.Abstractions.PickMediaOptions
                {
                    PhotoSize = Plugin.Media.Abstractions.PhotoSize.Medium,

                });


                if (file == null)
                    return;

                stream = file.GetStream();

                _circleImage.Source = ImageSource.FromStream(() =>
                {
                    var streams = file.GetStream();
                    //  file.Dispose();
                    return streams;
                });
            }
         


        }
        private string greeting;
        public string Greeting
        {
            get { return greeting; }
            set { SetProperty(ref greeting, value); }
        }

        public void YourGreeting(DateTime time)
        {

            if (time.Hour >= 0 && time.Hour < 12)
            {
                greeting = "Good Morning";
            }
            else if (time.Hour >= 12 && time.Hour < 18)
            {
                greeting = "Good Afternoon";
            }
            else if (time.Hour >= 18 && time.Hour <= 23)
            {
                greeting = "Good Evening";
            }

        }

        private DateTime date(string Datestring)
        {
            DateTime date = Convert.ToDateTime(Datestring);
            return date;
        }

        private async Task UploadAsync()
        {
            string filePath = file.Path;
            string fileName = Path.GetFileName(filePath);
            await cloudBlobContainer.CreateIfNotExistsAsync();

            await cloudBlobContainer.SetPermissionsAsync(new BlobContainerPermissions
            {
                PublicAccess = BlobContainerPublicAccessType.Blob
            });
            var blockBlob = cloudBlobContainer.GetBlockBlobReference(fileName);
            blobImageUploader = await UploadImage(blockBlob, filePath);


        }
        private static async Task<AzureImageObject> UploadImage(CloudBlockBlob blob, string filePath)
        {
            using (var fileStream = File.OpenRead(filePath))
            {
                await blob.UploadFromStreamAsync(fileStream);
            }

            AzureImageObject imageObject = new AzureImageObject()
            {
                pictureName = blob.ToString(),
                picturePath = blob.Uri.AbsoluteUri,
            };

            return imageObject;
        }

        private static async Task DownloadImage(CloudBlockBlob blob, string filePath)
        {
            if (blob.ExistsAsync().Result)
            {
                await blob.DownloadToFileAsync(filePath, FileMode.CreateNew);
            }
        }
    }
}
