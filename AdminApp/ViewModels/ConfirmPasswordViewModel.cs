﻿using AdminApp.MyInterfaces.IServices.Data;
using AdminApp.MyInterfaces.IServices.General;
using AdminApp.Views;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace AdminApp.ViewModels
{
    class ConfirmPasswordViewModel : BaseViewModel
    {
        private string _email;
        private long _reset;
        private string _newpassword;
        private string _Youremail;
    //    private string _confirmPassword;
      //  private string _passwordnotsame;
        private long Thereset;

        private IForgot _Recoverforgot;
        protected readonly IConnectionService _connectionService;
        private INavigation Navigation;
        public ConfirmPasswordViewModel(IConnectionService connectionService, INavigation navigation, IForgot forgot, long reset, string email)
        {
            _connectionService = connectionService;
            Navigation = navigation;
            _Recoverforgot = forgot;
            _email = email;
            Thereset = reset;
            _Youremail = email;
        }

        public string Email { get { return _email; } set { SetProperty(ref _email, value); } }

        public long Reset { get { return _reset; } set { SetProperty(ref _reset, value); } }

        public string NewPassword { get { return _newpassword; } set { SetProperty(ref _newpassword, value); } }
    //    public string ConfirmPassword { get { return _confirmPassword; } set { SetProperty(ref _confirmPassword, value); } }
       // public string Passwordnotsame { get { return _passwordnotsame; } set { SetProperty(ref _passwordnotsame, value); } }


        public ICommand Confirmcommand => new Command(Confirmnewpassword);

        private async void Confirmnewpassword()
        {
            if (_connectionService.IsConnected)
            {
                // var pass = Password();
                if (_Youremail == _email)
                {
                    if (Thereset == _reset)
                    {
                        var newpassword = await _Recoverforgot.confirmPassword(_email, _newpassword, _reset.ToString());
                        if (newpassword.Username == null)
                        {
                            await Application.Current.MainPage.DisplayAlert("New Password Created", "Your New Password has been created", "OK");
                            await Navigation.PushAsync(new Login(_email, _newpassword));
                        }

                    }
                    else
                    {
                        await Application.Current.MainPage.DisplayAlert("Invalid Code", "Please enter a valid code", "OK");
                    }
                }
                else
                {
                    await Application.Current.MainPage.DisplayAlert("Invalid Email", "Please enter a valid email", "OK");
                }
            }
            else
            {
                IsBusy = false;
                await Application.Current.MainPage.DisplayAlert("Check your connection", "No Network Connection Detected ", "OK");
            }
        }

        //private string Password()
        //{
        //  if (_newpassword == _confirmPassword)
        //    {
        //        return _newpassword;
        //    }
        //    else
        //    {
        //        return _passwordnotsame = "Password Mismathch";
        //    }
        //}
    }
}
