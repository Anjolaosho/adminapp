﻿using AdminApp.MyInterfaces.IServices.General;
using AdminApp.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AdminApp.ViewModels
{
    public class MainViewModel : ViewModelBase
    {
        //    private MenuViewModel _menuViewModel;

        public MainViewModel(IConnectionService connectionService,
            INavigationService navigationService, IDialogService dialogService)
            : base(connectionService, navigationService, dialogService)
        {
        }

        //    public MenuViewModel MenuViewModel
        //    {
        //        get => _menuViewModel;
        //        set
        //        {
        //            _menuViewModel = value;
        //            OnPropertyChanged();
        //        }
        //    }

        //    public override async Task InitializeAsync(object data)
        //    {
        //        await Task.WhenAll
        //        (
        //            _menuViewModel.InitializeAsync(data),
        //            _navigationService.NavigateToAsync<HomeViewModel>()
        //        );
        //    }
    }
}
