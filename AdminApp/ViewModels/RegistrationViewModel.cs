﻿using AdminApp.MyInterfaces.IServices;
using AdminApp.MyInterfaces.IServices.General;
using AdminApp.Services;
using AdminApp.ViewModels.Base;
using AdminApp.Views;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace AdminApp.ViewModels
{
    public class RegistrationViewModel : INotifyPropertyChanged
    {

        private readonly IAuthenticationService _authenticationService;
        protected readonly IConnectionService _connectionService;
        public INavigation Navigation { get; set; }

        private string _nameoforg;
        private string _password;
        private string _email;

        public RegistrationViewModel(IAuthenticationService authenticationService, INavigation navigation, IConnectionService ConnectionService)
        {
            _authenticationService = authenticationService;
            Navigation = navigation;
            _connectionService = ConnectionService; 
        }
        public event PropertyChangedEventHandler PropertyChanged;

        private bool _isBusy;
        public bool IsBusy
        {
            get => _isBusy;
            set
            {
                _isBusy = value;
                OnPropertyChanged(nameof(IsBusy));
            }
        }
        public string NameofOrg
        {
            get => _nameoforg;
            set
            {
                _nameoforg = value;
                OnPropertyChanged();
            }
        }
        public string Email
        {
            get => _email;
            set
            {
                _email = value;
                OnPropertyChanged();
            }
        }

        public string Password
        {
            get => _password;
            set
            {
                _password = value;
                OnPropertyChanged();
            }
        }

        private int check(string pass)
        {
            int count = 0; 
            foreach(var p in pass)
            {
                count++;
            }
            int k = count;
            return k;
        }

        public ICommand RegisterCommand => new Command(OnRegister);
        public ICommand LoginCommand => new Command(OnLogin);

        private async void OnRegister()
        {
            IsBusy = true;
            if (_connectionService.IsConnected)
            {
                if(_nameoforg != null)
                {
                    if (_email != null)
                    {
                        if(_password != null)
                        {
                            if (check(_password) > 6)
                            {
                                if (_email.Contains("@") && _email.Contains("."))
                                {
                                    try
                                    {
                                        var userRegistered = await
                                    _authenticationService.Register(_nameoforg, _email, _password);

                                        if (userRegistered.Username == null)
                                        {
                                            await Application.Current.MainPage.DisplayAlert("Invalid attempt", " email or UserName Invalid please change ", "OK");
                                            IsBusy = false;
                                        }
                                        else
                                        {
                                            if (String.IsNullOrEmpty(userRegistered.Token))
                                            {
                                                IsBusy = false;
                                                await Navigation.PushAsync(new Login(_email, _password, _nameoforg));
                                                Navigation.RemovePage(this.Navigation.NavigationStack[0]);
                                            }
                                        }
                                    }
                                    catch
                                    {
                                        await Application.Current.MainPage.DisplayAlert(
                                    "Something went wrong",
                                    "Error logging you in Please check your connection",
                                    "OK");
                                        IsBusy = false;
                                    }
                                   
                                }
                                else
                                {
                                    IsBusy = false;
                                    await Application.Current.MainPage.DisplayAlert("Invalid attempt", " Invalid Email ", "OK");
                                }
                            }
                            else
                            {
                                IsBusy = false;
                                await Application.Current.MainPage.DisplayAlert("Invalid attempt", " Password cannot be less than 6 characters ", "OK");
                            }
                        }
                        else
                        {
                            IsBusy = false;
                            await Application.Current.MainPage.DisplayAlert("Invalid attempt", " Password cannot be Empty ", "OK");
                        }

                    }
                    else
                    {
                        IsBusy = false;
                        await Application.Current.MainPage.DisplayAlert("Invalid attempt", " Email cannot be empty ", "OK");
                       
                    }
                }
                else
                {
                    IsBusy = false;
                    await Application.Current.MainPage.DisplayAlert("Invalid attempt", " Organisation Name Cannot be empty ", "OK");
                }
            }
            else
            {
                IsBusy = false;
                await Application.Current.MainPage.DisplayAlert("Check your connection", "No Network Connection Detected ", "OK");
            }
        }

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private void OnLogin()
        {
            Navigation.PushAsync(new Login());
        }
    }
}
