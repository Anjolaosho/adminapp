﻿using AdminApp.Container;
using AdminApp.Models;
using AdminApp.MyInterfaces.IServices;
using AdminApp.MyInterfaces.IServices.General;
using AdminApp.ViewModels.Base;
using AdminApp.Views;
using PCLStorage;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace AdminApp.ViewModels
{
    public class LoginViewModel : INotifyPropertyChanged
    {

        IFolder rootFolder = FileSystem.Current.LocalStorage;
        private readonly IAuthenticationService _authenticationService;
        protected readonly IConnectionService _connectionService;
        public INavigation Navigation { get; set; }
        protected readonly IDialogService _dialogService;

        private string authentication;
        private string NameofOrganisation;
        private string _email;
        private string _password;
        private string bank = "";
        byte[] bytes;
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public LoginViewModel(IAuthenticationService authenticationService, INavigation navigation,IDialogService DialogService,
           IConnectionService ConnectionService, string email, string password)
        {
            _authenticationService = authenticationService;
            _dialogService = DialogService;
         Navigation = navigation;
            _connectionService = ConnectionService;

            if (email != "" || password != "")
            {
                Email = email;
                Password = password;
            }

        }

        public ICommand LoginCommand => new Command(OnLogin);
        public ICommand RegisterCommand => new Command(OnRegister);

        public string Email
        {
            get => _email;
            set
            {
                _email = value;
                OnPropertyChanged();
            }
        }

        public string Password
        {
            get => _password;
            set
            {
                _password = value;
                OnPropertyChanged();
            }
        }

        private bool _isBusy;
        public bool IsBusy
        {
            get => _isBusy;
            set
            {
                _isBusy = value;
                OnPropertyChanged(nameof(IsBusy));
            }
        }

        private async void OnLogin()
        {
            IsBusy = true;
            if (_connectionService.IsConnected)
            {
                if(_email != null && _email.Contains("@") && _email.Contains("."))
                {
                    if(_password != null)
                    {
                        try
                        {
                            AuthenticationResponse authenticationResponse = new AuthenticationResponse();
                            authenticationResponse = await _authenticationService.Authenticate(Email, Password);
                            if (authenticationResponse == null)
                            {

                                await Application.Current.MainPage.DisplayAlert(
                                  "This username/password is invalid",
                                  "Error logging you in",
                                  "OK");

                            }
                            else
                            {

                                authentication = authenticationResponse.Token;
                                string username = authenticationResponse.Username;
                                var Userrole = authenticationResponse.UserRoles;
                                if (!(String.IsNullOrEmpty(authentication)))
                                {
                                    await getUser();
                                   IsBusy = false;
                                    await Navigation.PushAsync(new MainPage(authentication, Userrole, bank));
                                    Navigation.RemovePage(this.Navigation.NavigationStack[0]);

                                    //    await Navigation.PopAsync().ConfigureAwait(false);

                                }
                                else
                                {
                                    await Application.Current.MainPage.DisplayAlert(
                                        "This username/password is invalid",
                                        "Error logging you in",
                                        "OK");
                                    IsBusy = false;
                                }
                            }
                        }
                        catch(Exception)
                        {
                            await Application.Current.MainPage.DisplayAlert(
                                        "Something went wrong",
                                        "Error logging you in Please check your connection",
                                        "OK");
                            IsBusy = false;
                        }
                       
                    }
                    else
                    {
                        await Application.Current.MainPage.DisplayAlert(
                              "This password is invalid",
                              "Error logging you in",
                              "OK");
                        IsBusy = false;
                    }
                   
                }
                else
                {
                    await Application.Current.MainPage.DisplayAlert(
                                "This username is invalid",
                                "Error logging you in",
                                "OK");
                    IsBusy = false;
                }
               
            }

            else
            {
                IsBusy = false;
                await Application.Current.MainPage.DisplayAlert("Check your connection","No Network Connection Detected ", "OK");
            }
            
        }

        private void OnRegister()
        {
            Navigation.PushAsync(new SignIn());
            Navigation.RemovePage(this.Navigation.NavigationStack[0]);
        }

        private async Task getUser()
        {
            var tenant = await _authenticationService.gettenant(authentication);
            bank = tenant.Bank;
            NameofOrganisation = tenant.NameOfOrganization;
            var Orglogo = tenant.LogoBlobUrl;
            if (!string.IsNullOrEmpty(Orglogo))
            {
                bytes = Encoding.ASCII.GetBytes(Orglogo);
            }

                await PCLHelper.CreateFolder("Pipu", rootFolder);
                var b = await rootFolder.CheckExistsAsync("Pipu");

                if (b != ExistenceCheckResult.FolderExists)
                {
                    await PCLHelper.CreateFolder("Pipu", rootFolder);
                }

                var root = await rootFolder.GetFolderAsync("Pipu");

                var existence = await root.CheckExistsAsync("AuthFile");

                if (existence != ExistenceCheckResult.FileExists)
                {
                    await PCLHelper.CreateFile("AuthFile", root);
                    await PCLHelper.WriteTextAllAsync("AuthFile", NameofOrganisation,root);
                }

                if (!(await PCLHelper.IsFileExistAsync("Logo", root)))
                {
                    await PCLHelper.CreateFile("Logo", root);
                    if (Orglogo != null)
                    {
                        await PCLHelper.SaveImage(bytes, "Logo", root);
                    }

                }
                else
                {
                    await PCLHelper.SaveImage(bytes, "Logo");
                }

            // we store the Id to know if the user is already logged in to the application
            Settings.Email = tenant.Email;
            Settings.Password = _password;
            Settings.Bank = bank;

        }

    }
}
