﻿using AdminApp.Container;
using AdminApp.Views;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace AdminApp.ViewModels
{
    class HomeViewModel : BaseViewModel
    {
        string token;
        public INavigation _navigation { get; set; }
        private string greeting;
        public HomeViewModel(string authentication, INavigation Navigation)
        {
            token = authentication;
            YourGreeting(DateTime.Now);
            _navigation = Navigation;
        }


        public string Greeting
        {
            get { return greeting; }
            set { SetProperty(ref greeting, value); }
        }

        public void YourGreeting(DateTime time)
        {

            if (time.Hour >= 0 && time.Hour < 12)
            {
                greeting = "Good Morning";
            }
            else if (time.Hour >= 12 && time.Hour < 18)
            {
                greeting = "Good Afternoon";
            }
            else if (time.Hour >= 18 && time.Hour <= 23)
            {
                greeting = "Good Evening";
            }
        }
             public ICommand logout => new Command(Logout);

         private async void Logout()
        {
           var signout = await Application.Current.MainPage.DisplayAlert("Signout", $" Sign out now ?", "Yes", "No");

            if (signout)
            {
                token = "";
                Settings.Password = "";
                Settings.Email = "";
                Settings.Bank = "";
                App.Current.MainPage = new NavigationPage(new Login()); 
            }
        }
    }
}
