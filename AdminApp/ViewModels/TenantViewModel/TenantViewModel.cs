﻿using AdminApp.Container;
using AdminApp.Views;
using PCLStorage;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace AdminApp.ViewModels.TenantViewModel
{
    class TenantViewModel : BaseViewModel
    {
        string token;
        public INavigation _navigation { get; set; }
        private string Bank;
        private string greeting;
        public TenantViewModel(string authentication,string bank,INavigation Navigation)
        {
            token = authentication;
            YourGreeting(DateTime.Now);
            Bank = bank;
            Removewarning(bank);
            _navigation = Navigation;

        }

        public ICommand Back => new Command(BackTapped);

        private void BackTapped()
        {
            _navigation.PopAsync();
        }

        private string orgimage;
        public string Orgimage { get { return orgimage; } set { SetProperty(ref orgimage, value); } } 
        
       
        private bool updateWarning;
        public bool UpdateWarning { get { return updateWarning; } set { SetProperty(ref updateWarning, value); } }

        public string Greeting
        {
            get { return greeting; }
            set { SetProperty(ref greeting, value); }
        }


        internal void Removewarning(string bank)
        {
            if (string.IsNullOrWhiteSpace(bank))
            {
                updateWarning = true;
            }
            else
            {
                updateWarning = false;
            }


        }

        public void YourGreeting(DateTime time)
        {

            if (time.Hour >= 0 && time.Hour < 12)
            {
                greeting = "Good Morning";
            }
            else if (time.Hour >= 12 && time.Hour < 18)
            {
                greeting = "Good Afternoon";
            }
            else if (time.Hour >= 18 && time.Hour <= 23)
            {
                greeting = "Good Evening";
            }
        }

        public ICommand logout => new Command(Logout);

        private async void Logout()
        {
            var signout = await Application.Current.MainPage.DisplayAlert("Signout", $" Sign out now ?", "Yes", "No");

            if (signout)
            {
                token = "";
                Settings.Password = "";
                Settings.Email = "";
                Settings.Bank = "";
                App.Current.MainPage = new NavigationPage(new Login());
            }
        }
    }
}
