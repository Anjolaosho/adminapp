﻿using AdminApp.Models;
using AdminApp.MyInterfaces.IServices.Data;
using AdminApp.MyInterfaces.IServices.General;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace AdminApp.ViewModels.TenantViewModel
{
    class UserTenantViewModel : BaseViewModel
    {
        private readonly IUsertenant userTenant;
        protected readonly IConnectionService _connectionService;
        private string Token;

        public INavigation _navigation { get; set; }

        private string _name;

        private string _email;
        public UserTenantViewModel(IConnectionService connectionService, INavigation Navigation, IUsertenant _userTenant, string authenticationResponse)
        {
            _connectionService = connectionService;
            _navigation = Navigation;
            userTenant = _userTenant;
            Token = authenticationResponse;
            YourGreeting(DateTime.Now);
        }
        public string Name
        {
            get { return _name; }
            set { SetProperty(ref _name, value); }
        }

        public string Email
        {
            get { return _email; }
            set { SetProperty(ref _email, value); }
        }

        public ICommand Back => new Command(BackTapped);

        private void BackTapped()
        {
            _navigation.PopAsync();
        }

        public ICommand sendcommand => new Command(Send);

        private async void Send()
        {
            try
            {
                IsBusy = true;
                if (_connectionService.IsConnected)
                {
                    if(_name != null)
                    {
                        if(_email != null)
                        {
                            if(_email.Contains("@") && _email.Contains("."))
                            {
                                try
                                {
                                    IsBusy = true;
                                    var UserTenantadded = await userTenant.tenantUser(_name, _email, Token);

                                    await Application.Current.MainPage.DisplayAlert("Admin Added successfully", " ", "OK");
                                    IsBusy = false;
                                    await _navigation.PopAsync();
                                }
                                catch(Exception)
                                {
                                    IsBusy = false;
                                    await Application.Current.MainPage.DisplayAlert("Invalid", "Invalid Input", "OK");
                                }
                              
                            }
                            else
                            {
                                await Application.Current.MainPage.DisplayAlert("Invalid", "Invalid Email", "OK");
                            }
                            
                        }
                        else
                        {
                            await Application.Current.MainPage.DisplayAlert("Invalid", "Email Cannot be null", "OK");
                        }
                       
                    }
                    else
                    {
                        await Application.Current.MainPage.DisplayAlert("Invalid", "Name Cannot be null", "OK");
                    }
                    
                }
                else
                {
                    IsBusy = false;
                    await Application.Current.MainPage.DisplayAlert("Check your connection", "No Network Connection Detected ", "OK");
                }

            }
            catch
            {
              
            }

        }

        private string greeting;
        public string Greeting
        {
            get { return greeting; }
            set { SetProperty(ref greeting, value); }
        }

        public void YourGreeting(DateTime time)
        {

            if (time.Hour >= 0 && time.Hour < 12)
            {
                greeting = "Good Morning";
            }
            else if (time.Hour >= 12 && time.Hour < 18)
            {
                greeting = "Good Afternoon";
            }
            else if (time.Hour >= 18 && time.Hour <= 23)
            {
                greeting = "Good Evening";
            }

        }
    }
}

