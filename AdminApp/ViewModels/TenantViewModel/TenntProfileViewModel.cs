﻿using AdminApp.constants;
using AdminApp.Container;
using AdminApp.Models;
using AdminApp.MyInterfaces.IRepository;
using AdminApp.MyInterfaces.IServices;
using AdminApp.MyInterfaces.IServices.Data;
using AdminApp.MyInterfaces.IServices.General;
using AdminApp.Repository;
using AdminApp.Services;
using AdminApp.Views;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using PCLStorage;
using Plugin.Media;
using Plugin.Media.Abstractions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace AdminApp.ViewModels.TenantViewModel
{
    class TenntProfileViewModel  : BaseViewModel
    {
        MediaFile file;
        static string _storageConnection = "DefaultEndpointsProtocol=https;AccountName=churchplusstorage;AccountKey=rQZhE5UYZ9EdgzVZvr3bXLMNYEuoQG3jGW71uQFVeKxI+YR3iBlRyLWMxqOGTT83L3/6jRBH8uSSZkC3oJ+duA==;EndpointSuffix=core.windows.net";
        static CloudStorageAccount cloudStorageAccount = CloudStorageAccount.Parse(_storageConnection);
        static CloudBlobClient cloudBlobClient = cloudStorageAccount.CreateCloudBlobClient();
        static CloudBlobContainer cloudBlobContainer = cloudBlobClient.GetContainerReference("membershipmanager");



        private readonly ITenant _tenant;
        protected readonly IConnectionService _connectionService;
        private readonly IGenericRepository _genericRepository;

        IFolder folder = FileSystem.Current.LocalStorage;
        string PipuFolder = "PipuFolder";

        Stream stream = null;

        string Token;

        private INavigation _navigation;

        private string name;

        private string email;

        private string password;

        private string accountNumber;

        private string bank;

        private string dateEstablished;
        public FileImageSource imagefile;
        private string logoPath; 
        private string logoBlobUrl;
        private string address;
       AzureImageObject blobImageUploader;
        public Task<List<string>> Thebanks { get; set; }

        public TenntProfileViewModel(IGenericRepository genericRepository, IConnectionService connectionService, INavigation Navigation, ITenant tenant, string authenticationResponse)
        {
            _connectionService = connectionService;
            _navigation = Navigation;
            _tenant = tenant;
            _genericRepository = genericRepository;
            Token = authenticationResponse;
            blobImageUploader = new AzureImageObject();
            YourGreeting(DateTime.Now);
        }

        public string Name { get { return name; } set { SetProperty(ref name, value); } }

        public string EmailAddress { get { return email; } set { SetProperty(ref email, value); } }

        public string Password { get { return password; } set { SetProperty(ref password, value); } }

        public string AccountNo { get { return accountNumber; } set { SetProperty(ref accountNumber, value); } }

        public string Banklist { get { return bank; } set { SetProperty(ref bank, value); } }

        public string DateEstablished { get { return dateEstablished; } set { SetProperty(ref dateEstablished, value); } }

        public FileImageSource Logo { get { return imagefile; } set { SetProperty(ref imagefile, value); } }


        // LOGO
        private Xamarin.Forms.Image userLogo = new Xamarin.Forms.Image(); 
        public Xamarin.Forms.Image UserLogo { get { return userLogo; } set { SetProperty(ref userLogo, value); } } 



        public string LogoSource { get { return logoPath; } set {SetProperty(ref logoPath,value); } }

        public string LogoBlobUrl { get { return logoBlobUrl; } set { SetProperty(ref logoBlobUrl, value); } }

        public string Address { get { return address; } set { SetProperty(ref address, value); } }

        public MultipartFormDataContent content { get; set; }

        public ICommand sendcommand => new Command(Send);
        public ICommand Pickcommand => new Command(ChoosePicture);

        public ICommand Back => new Command(BackTapped);

        private void BackTapped()
        {
            _navigation.PopAsync();
        }
        private async void Send()
        {
            try
            {

                if (_connectionService.IsConnected)
                {
                   
                    if (name != null && email != null && accountNumber != null &&
                bank != null)
                    {
                        if(accountNumber.Length == 10)
                        {
                            // make sure there is an image in the file
                            if (file != null)
                            {
                                try
                                {
                                    IsBusy = true;
                                    await Application.Current.MainPage.DisplayAlert("Profile Updating", $"Your Profile will be updated soon", "OK");
                                    await uploadAsync();
                                    var updatede = await _tenant.updateTenant(date(dateEstablished), name, email, accountNumber,
                                 bank, address, blobImageUploader, Token);
                                    IsBusy = false;
                                    await Application.Current.MainPage.DisplayAlert("Profile Updated", $"Enjoy managing your Pipu ", "OK");
                                    App.Current.MainPage = new NavigationPage(new Login(email));
                                    await Application.Current.MainPage.DisplayAlert("Login", $"Thank you for completing your profile. Please enter your password ", "OK");
                                }
                                catch(Exception)
                                {
                                    IsBusy = false;
                                    await Application.Current.MainPage.DisplayAlert("Invalid Input", $"Please Make sure all fields have been entered correctly and make sure email has not ben used before ", "OK");
                                }
                            }
                            else
                            {
                             var input = await Application.Current.MainPage.DisplayAlert("No Image", $"Are you sure you dont want to add an image ", "Yes", "No");
                                if (input)
                                {
                                    try
                                    {
                                        IsBusy = true;
                                        await Application.Current.MainPage.DisplayAlert("Profile Updating", $"Your Profile will be updated soon", "OK");
                                        var updatede = await _tenant.updateTenant(date(dateEstablished), name, email, accountNumber,
                                     bank, address, blobImageUploader, Token);
                                        await Application.Current.MainPage.DisplayAlert("Profile Updated", $"Enjoy managing your Pipu ", "OK");
                                        App.Current.MainPage = new NavigationPage(new Login(email));
                                        await Application.Current.MainPage.DisplayAlert("Login", $"Thank you for completing your profile. Please enter your  Details again ", "OK");
                                    }
                                    catch (Exception)
                                    {
                                        IsBusy = false;
                                        await Application.Current.MainPage.DisplayAlert("Invalid Input", $"Please Make sure all fields have been entered correctly and make sure email has not ben used before ", "OK");
                                    }
                                   
                                }
                             
                            }
                        }
                        else
                        {
                            await Application.Current.MainPage.DisplayAlert("Invalid Account number", $"Account number invaild", "OK");
                        }
                        
                    }
                    else
                    {
                        await Application.Current.MainPage.DisplayAlert("One or more important fields is empty"," The Important fields are nameOfOrganization, email, accountNumber, bank", "Please enter");
                    }
                    
                }
                else
                {
                    await Application.Current.MainPage.DisplayAlert("No Connection", $"Please check internet connection ", "OK");
                }
            }
            catch 
            {
                await Application.Current.MainPage.DisplayAlert("No Connection", $"Please check internet connection ", "OK");
            }

        }

        public async void ChoosePicture()
        {

            await CrossMedia.Current.Initialize();

            var pick = await Application.Current.MainPage.DisplayAlert("Get Picture", $"Do you want to Pick photo from Gallery or take photo with camera ", "Use Camera", "Use Gallery");


            if (pick)
            {
                if (!CrossMedia.Current.IsCameraAvailable ||
        !CrossMedia.Current.IsTakePhotoSupported)
                {
                    await Application.Current.MainPage.DisplayAlert("No Camera", "No camera available.", "OK");
                    return;
                }
                file = await CrossMedia.Current.TakePhotoAsync(
               new Plugin.Media.Abstractions.StoreCameraMediaOptions
               {
                   Directory = "Sample",

                   Name = "test.jpg"
               });
                if (file == null)
                    return;


                //  await Application.Current.MainPage.DisplayAlert("File Location", file.Path, "OK");

                UserLogo.Source = ImageSource.FromStream(() =>
                {
                    var stream = file.GetStream();
                    //  file.Dispose();
                    return stream;
                });
            }
            else
            {
                if (!CrossMedia.Current.IsPickPhotoSupported)
                {
                    await Application.Current.MainPage.DisplayAlert("Photos Not Supported", ":( Permission not granted to photos.", "OK");
                    return;
                }
                file = await Plugin.Media.CrossMedia.Current.PickPhotoAsync(new Plugin.Media.Abstractions.PickMediaOptions
                {
                    PhotoSize = Plugin.Media.Abstractions.PhotoSize.Medium,

                });


                if (file == null)
                    return;

                stream = file.GetStream();

                UserLogo.Source = ImageSource.FromStream(() =>
                {
                    var streams = file.GetStream();
                //  file.Dispose();
                return streams;
                });
            }

        }

        private DateTime date(string Datestring)
        {
            DateTime date = Convert.ToDateTime(Datestring);
            return date;
        }
           
        
        private string greeting;
        public string Greeting
        {
            get { return greeting; }
            set { SetProperty(ref greeting, value); }
        }

        public void YourGreeting(DateTime time)
        {

            if (time.Hour >= 0 && time.Hour < 12)
            {
                greeting = "Good Morning";
            }
            else if (time.Hour >= 12 && time.Hour < 18)
            {
                greeting = "Good Afternoon";
            }
            else if (time.Hour >= 18 && time.Hour <= 23)
            {
                greeting = "Good Evening";
            }

        }

        private async Task uploadAsync()
        {
            string filePath = file.Path;
            string fileName = Path.GetFileName(filePath);
            await cloudBlobContainer.CreateIfNotExistsAsync();

            await cloudBlobContainer.SetPermissionsAsync(new BlobContainerPermissions
            {
                PublicAccess = BlobContainerPublicAccessType.Blob
            });
            var blockBlob = cloudBlobContainer.GetBlockBlobReference(fileName);
            blobImageUploader =  await UploadImage(blockBlob, filePath);


        }
        private static async Task<AzureImageObject> UploadImage(CloudBlockBlob blob, string filePath)
        {
            using (var fileStream = File.OpenRead(filePath))
            {
                await blob.UploadFromStreamAsync(fileStream);
            }

            AzureImageObject imageObject = new AzureImageObject()
            {
                pictureName = blob.ToString(),
                picturePath = blob.Uri.AbsoluteUri,
            };

            return imageObject;
        }

        private static async Task DownloadImage(CloudBlockBlob blob, string filePath)
        {
            if (blob.ExistsAsync().Result)
            {
                await blob.DownloadToFileAsync(filePath, FileMode.CreateNew);
            }
        }

       


    }
}
