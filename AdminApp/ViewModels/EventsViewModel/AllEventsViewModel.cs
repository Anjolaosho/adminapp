﻿using AdminApp.Models;
using AdminApp.Models.EventsModel;
using AdminApp.MyInterfaces.IServices.Data;
using AdminApp.MyInterfaces.IServices.General;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace AdminApp.ViewModels.EventsViewModel
{
    class AllEventsViewModel : BaseViewModel
    {
        private readonly IEvents _events;
        public INavigation Navigation { get; set; }
        protected readonly IConnectionService _connectionService;
        private string Token;

       private List<Events> myevents = new List<Events>();

        private ObservableCollection<Events> getEvents;
      public  ObservableCollection<Events> GetEvents { get { return getEvents;  } set { SetProperty(ref getEvents, value); } }
        public AllEventsViewModel(IConnectionService connectionService, INavigation navigation,
            IEvents Events, string authenticationResponse)
        {
            Token = authenticationResponse;
            _connectionService = connectionService;
            _events = Events;
            Navigation = navigation;
            YourGreeting(DateTime.Now);
        }

        private bool _isRefreshing = false;
        public bool IsRefreshing
        {
            get { return _isRefreshing; }
            set
            {
                SetProperty(ref _isRefreshing, value);
                
            }
        }

        public ICommand RefreshCommand
        {
            get
            {
                return new Command(async () =>
                {
                    IsRefreshing = true;

                    await RefreshData();

                    IsRefreshing = false;
                });
            }
        }

        private async Task RefreshData()
        {
            await InitializeAsync();
            getEvents = new ObservableCollection<Events>(myevents);
        }


        public ICommand Back => new Command(BackTapped);

        private void BackTapped()
        {
            Navigation.PopAsync();
        }

        public async Task<List<Events>> InitializeAsync()
        {
            IsBusy = true;

          var getevents =   await _events.GetAllEventsAsync(Token);

            IsBusy = false;
         //   myevents = getevents;
            return getevents;
        }

        public async void EventsAsync()
        {
            myevents = await InitializeAsync();
            getEvents = new ObservableCollection<Events>(myevents);
        }

        private string greeting;
        public string Greeting
        {
            get { return greeting; }
            set { SetProperty(ref greeting, value); }
        }

        public void YourGreeting(DateTime time)
        {

            if (time.Hour >= 0 && time.Hour < 12)
            {
                greeting = "Good Morning";
            }
            else if (time.Hour >= 12 && time.Hour < 18)
            {
                greeting = "Good Afternoon";
            }
            else if (time.Hour >= 18 && time.Hour <= 23)
            {
                greeting = "Good Evening";
            }

        }
    }
    
}
