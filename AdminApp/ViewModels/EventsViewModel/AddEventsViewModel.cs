﻿using AdminApp.Models;
using AdminApp.Models.EventsModel;
using AdminApp.MyInterfaces.IServices;
using AdminApp.MyInterfaces.IServices.Data;
using AdminApp.MyInterfaces.IServices.General;
using Plugin.Media;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace AdminApp.ViewModels.EventsViewModel
{
     class AddEventsViewModel : BaseViewModel
    {
        private readonly IEvents _events;
        protected readonly IConnectionService _connectionService;
        private Page page; private string Token;

        public INavigation _navigation { get; set; }

        private string _id;

        private string _tenantId;

        private string _title;

        private string _bannerPath;

        private string BannerBlobUrl;

        private string _details;

        private string _venue;

        private string _mapUrl;

        private TimeSpan _start;

        private TimeSpan _end;

        

        public AddEventsViewModel(IConnectionService connectionService, INavigation Navigation, IEvents events, string authenticationResponse)
        {
            _connectionService = connectionService;
            _navigation = Navigation;
            _events = events;
            Token = authenticationResponse;
            YourGreeting(DateTime.Now);


        }

        public string Id
        {
            get { return _id; }
            set { SetProperty(ref _id, value); }
        }

        public string TenantId
        {
            get { return _tenantId; }
            set { SetProperty(ref _tenantId, value); }
        }

        public string ETitle
        {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }

        public string BannerPath
        {
            get { return _bannerPath; }
            set { SetProperty(ref _bannerPath, value); }
        }

        public string Details
        {
            get { return _details; }
            set { SetProperty(ref _details, value); }
        }

        public string Venue
        {
            get { return _venue; }
            set { SetProperty(ref _venue, value); }
        } 

        public string MapUrl
        {
            get { return _mapUrl; }
            set { SetProperty(ref _mapUrl, value); }
        }

        private DateTime _startDate;
        public DateTime StartDate
        {
            get { return _startDate; }
            set { SetProperty(ref _startDate, value); }
        }

        private DateTime _endDate;
        public DateTime EndDate
        {
            get { return _endDate; }
            set { SetProperty(ref _endDate, value); }
        }
        
        public TimeSpan Start
        {
            get { return _start; }
            set { SetProperty(ref _start, value); }
        }

        public TimeSpan End
        {
            get { return _end; }
            set { SetProperty(ref _end, value); }
        }



        private Xamarin.Forms.Image _circleImage = new Xamarin.Forms.Image();
        public Xamarin.Forms.Image circleImage
        {
            get { return _circleImage; }
            set { SetProperty(ref _circleImage, value); }
        }


        private DateTime thedateTime(DateTime date, TimeSpan time)
        {

            DateTime arrival = new DateTime(date.Year, date.Month, date.Day, time.Hours, time.Minutes, time.Seconds);
            return arrival; 
        }

        public ICommand Back => new Command(BackTapped);

        private void BackTapped()
        {
            _navigation.PopAsync();
        }

        public MultipartFormDataContent content { get; set; }

        public ICommand sendcommand => new Command(Send);

        public ICommand picturecommand => new Command(Takepicture);

        private async void Send()
        {
            try
            {
                if (_connectionService.IsConnected)
                {
                    if(_title != null && Details != null)
                    {
                        if(_startDate != null && _endDate != null)
                        {
                            if(_start != null && _end != null)
                            {
                                
                                try
                                {
                                    IsBusy = true;
                                    await Application.Current.MainPage.DisplayAlert("Adding Event", $" Your event will be added shortely", "OK");
                                    Event addevent = new Event()
                                    {
                                        Title = _title,
                                        // BannerPath = _bannerPath,
                                        MapUrl = _mapUrl,
                                        Details = _details,
                                        Venue = _venue,

                                        StartTime = thedateTime(_startDate, _start),
                                        EndTime = thedateTime(_endDate, _end),
                                    };
                                    if(addevent.StartTime.Date <= addevent.EndTime.Date)
                                    {
                                        var eventadded = await _events.@event(addevent, Token);
                                        IsBusy = false;
                                        await Application.Current.MainPage.DisplayAlert("Event Added", $"{addevent.Title} has been added ", "OK");
                                        await _navigation.PopAsync();
                                    }
                                    else
                                    {
                                        IsBusy = false;
                                        await Application.Current.MainPage.DisplayAlert("Input Error!", $"Start date cannot be after End date", "OK");
                                    }

                                  
                                }
                                catch (Exception)
                                {
                                    IsBusy = false;
                                    await Application.Current.MainPage.DisplayAlert("Invalid input", $" One or more fields are incorrect", "OK");
                                }
                            }
                            else
                            {
                                IsBusy = false;
                                await Application.Current.MainPage.DisplayAlert("Time not Added", $" Start time or end time empty please input a value", "OK");
                            }
                          
                        }
                        else
                        {
                            IsBusy = false;
                            await Application.Current.MainPage.DisplayAlert("Date not Added", $" Start date or end date empty please input a value", "OK");
                        }
                       
                    }
                    else
                    {
                        IsBusy = false;
                        await Application.Current.MainPage.DisplayAlert("Error", $"Title or Details empty ", "OK");
                    }
                }
                else { 
                await Application.Current.MainPage.DisplayAlert("No Connection", $"Please check internet connection ", "OK");
                }

            }
            catch 
            {
                
            }

        }

        public async void Takepicture()
        {
            page = new Page();


            await CrossMedia.Current.Initialize();

            if (!CrossMedia.Current.IsCameraAvailable ||
             !CrossMedia.Current.IsTakePhotoSupported)
            {
                await Application.Current.MainPage.DisplayAlert("No Camera", "No camera available.", "OK");
                return;
            }
            var file = await CrossMedia.Current.TakePhotoAsync(
            new Plugin.Media.Abstractions.StoreCameraMediaOptions
            {
                Directory = "Sample",

                Name = "test.jpg"
            });
            if (file == null)
                return;


            await Application.Current.MainPage.DisplayAlert("File Location", file.Path, "OK");

            _bannerPath = file.Path;

            _circleImage.Source = ImageSource.FromStream(() =>
            {
                var stream = file.GetStream();
                file.Dispose();
                return stream;
            });

        }
        private string greeting;
        public string Greeting
        {
            get { return greeting; }
            set { SetProperty(ref greeting, value); }
        }

        public void YourGreeting(DateTime time)
        {

            if(time.Hour >= 0 && time.Hour < 12)
            {
                greeting = "Good Morning";
            }
            else if(time.Hour >= 12 && time.Hour < 18)
            {
                greeting = "Good Afternoon";
            }
            else if(time.Hour >= 18 && time.Hour <= 23)
            {
                greeting = "Good Evening";
            }

        }

        public ICommand Maps => new Command(maps);

        public async void maps()
        {
            try
            {
                await Application.Current.MainPage.DisplayAlert("Instruction", $" Type the address on google maps, copy the url by going to share and then 'copy to' Then paste the url in the Map Url entry field", "OK");

                var location = await Geolocation.GetLastKnownLocationAsync();
                if (location != null)
                {
                    double latitude = location.Latitude;
                    double longitude = location.Longitude;
                    string placeName = "Home";

                    var supportsUri = await Launcher.CanOpenAsync("comgooglemaps://");

                    if (supportsUri)
                        await Launcher.OpenAsync($"comgooglemaps://?q={latitude},{longitude}({placeName})");
                    else
                        await Map.OpenAsync(latitude, longitude, new MapLaunchOptions { Name = "Test" });
                }

            }
            catch (FeatureNotSupportedException)
            {
                // Handle not supported on device exception  
                await Application.Current.MainPage.DisplayAlert("Maps not supported", "Your Device does not support Location.", "OK");
            }
            catch (PermissionException)
            {
                // Handle permission exception  
                await Application.Current.MainPage.DisplayAlert("Access Denied", "You have not given this app permission to access your location", "OK");
            }
            catch (Exception)
            {
                // Unable to get location  
                await Application.Current.MainPage.DisplayAlert("Location Error", "Unable to get location.", "OK");
            }
        } 
        
    }

}
