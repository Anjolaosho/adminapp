﻿using AdminApp.Models;
using AdminApp.Models.NoticeModel;
using AdminApp.MyInterfaces.IServices;
using AdminApp.MyInterfaces.IServices.Data;
using AdminApp.MyInterfaces.IServices.General;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace AdminApp.ViewModels.NoticesViewModel
{
    public class AddNoticesViewModel : BaseViewModel
    {
        private readonly INotices _notices;
        protected readonly IConnectionService _connectionService;
        private string Token;

        public INavigation _navigation { get; set; }

        private string _subject;

        private string _message;
        public AddNoticesViewModel(IConnectionService connectionService, INavigation Navigation, INotices Notices, string authenticationResponse)
        {
            _connectionService = connectionService;
            _navigation = Navigation;
            _notices = Notices;
            Token = authenticationResponse;
            YourGreeting(DateTime.Now);
        }
        public string Subject
        {
            get { return _subject; }
            set { SetProperty(ref _subject, value); }
        }

        public string Message
        {
            get { return _message; }
            set { SetProperty(ref _message, value); }
        }

        public ICommand Back => new Command(BackTapped);

        private void BackTapped()
        {
            _navigation.PopAsync();
        }
        public ICommand sendcommand => new Command(Send);

        private async void Send()
        {
            try
            {
                if (_connectionService.IsConnected)
                {
                    if(_subject != null && _message != null)
                    {
                        IsBusy = true;
                        await Application.Current.MainPage.DisplayAlert("Sending Notice", $" Your notice will be sent shortely", "OK");
                        Notices addnotice = new Notices()
                        {
                            Subject = _subject,
                            Message = _message,
                            //DateSent = DateTime.Now,
                            //  Tenant = new Tenant(),
                        };

                        var noticeadded = await _notices.notices(addnotice, Token);
                        IsBusy = false;
                        await Application.Current.MainPage.DisplayAlert("Notice Sent", " ", "OK");

                        await _navigation.PopAsync();
                    }
                    else
                    {
                        await Application.Current.MainPage.DisplayAlert("Incomplete", "Please Enter all fields ", "OK");
                    }
                   
                }
                else
                {
                    IsBusy = false;
                    await Application.Current.MainPage.DisplayAlert("Check your connection", "Please Check Network Connection ", "OK");
                }

            }
            catch
            {
                
            }

        }

        private string greeting;
        public string Greeting
        {
            get { return greeting; }
            set { SetProperty(ref greeting, value); }
        }

        public void YourGreeting(DateTime time)
        {

            if (time.Hour >= 0 && time.Hour < 12)
            {
                greeting = "Good Morning";
            }
            else if (time.Hour >= 12 && time.Hour < 18)
            {
                greeting = "Good Afternoon";
            }
            else if (time.Hour >= 18 && time.Hour <= 23)
            {
                greeting = "Good Evening";
            }

        }
    }
}
