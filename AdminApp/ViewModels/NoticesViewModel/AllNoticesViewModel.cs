﻿using AdminApp.Models;
using AdminApp.Models.NoticeModel;
using AdminApp.MyInterfaces.IServices.Data;
using AdminApp.MyInterfaces.IServices.General;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace AdminApp.ViewModels.NoticesViewModel
{
    class AllNoticesViewModel : BaseViewModel
    {
        private ObservableCollection<Notices> allNotices { get; set; }

        private readonly INotices _notices;
        public INavigation Navigation { get; set; }
        protected readonly IConnectionService _connectionService;
        private Page page;
        private string Token;

        public AllNoticesViewModel(IConnectionService connectionService, INavigation navigation,
            INotices Notices, string authenticationResponse)
        {
            _connectionService = connectionService;
            InitializeAsync();
            _notices = Notices;
            Navigation = navigation;
            Token = authenticationResponse;
            YourGreeting(DateTime.Now);
        }

      
        public ICommand Back => new Command(BackTapped);

        private void BackTapped()
        {
            Navigation.PopAsync();
        }
        public ObservableCollection<Notices> Events
        {
            get => allNotices;
            set
            {
                allNotices = value;
                OnPropertyChanged();
            }
        }

        public async Task InitializeAsync()
        {
            IsBusy = true;

            await _notices.GetAllNoticeAsync(Token);

            IsBusy = false;
        }

        private string greeting;
        public string Greeting
        {
            get { return greeting; }
            set { SetProperty(ref greeting, value); }
        }

        public void YourGreeting(DateTime time)
        {

            if (time.Hour >= 0 && time.Hour < 12)
            {
                greeting = "Good Morning";
            }
            else if (time.Hour >= 12 && time.Hour < 18)
            {
                greeting = "Good Afternoon";
            }
            else if (time.Hour >= 18 && time.Hour <= 23)
            {
                greeting = "Good Evening";
            }

        }
    }
}
