﻿using AdminApp.Models.MembersModel;
using AdminApp.Models.PaymentsModel;
using AdminApp.MyInterfaces.IRepository;
using AdminApp.MyInterfaces.IServices.Data;
using AdminApp.MyInterfaces.IServices.General;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace AdminApp.ViewModels.PaymentsViewModel
{
    class SuscribeMemberViewModel : BaseViewModel
    {
        protected readonly IConnectionService _connectionService;
        private readonly IGenericRepository _genericRepository;
        private readonly IPayments _payments;
        private INavigation _navigation;
        string Token;
        public SuscribeMemberViewModel(IGenericRepository genericRepository, IConnectionService connectionService, INavigation Navigation, IPayments payments, string authenticationResponse,string name)
        {
            _connectionService = connectionService;
            _navigation = Navigation;
            _genericRepository = genericRepository;
            Token = authenticationResponse;
            _payments = payments;
            member = new Member();
            member.FullName = name;
            YourGreeting(DateTime.Now);
        }

        private Member member;

        public Member Member
        {
            get { return member; }
            set { SetProperty(ref member, value); }
        }
          
        private Payment payment;

        public Payment Payments
        {
            get { return payment; }
            set { SetProperty(ref payment, value); }
        }


        private string greeting;
        public string Greeting
        {
            get { return greeting; }
            set { SetProperty(ref greeting, value); }
        }


        // The subscription count for each payment
        private int TheCount(Payment payment)
        {
               
            if (payment.PaymentType == Pay.onetime)
            {
                return 1;
            }
            else if(payment.PaymentType == Pay.recurring)
            {
                if(payment.BillingCycle == Cycle.weekly)
                {
                    return 1;
                }
                else if(payment.BillingCycle == Cycle.monthly)
                {
                    return 2;
                }
                else if(payment.BillingCycle == Cycle.biannually)
                {
                    return 3; 
                }
                else if(payment.BillingCycle == Cycle.annually)
                {
                    return 4;
                }
                else
                {
                    return 1;
                }
            }
            else
            {
                return 1;
            }
        }


        public ICommand MakePayment => new Command(pay);

        private async void pay()
        {
            try
            {
                if (_connectionService.IsConnected)
                {
                    IsBusy = true;
                    MainSub main = new MainSub()
                    {
                        MemberId = member.Id,
                        PaymentId = payment.Id,
                        AmountPaid = payment.Amount,
                        isActive = member.IsActive,
                        Reference = null,
                        subscriptionCount = TheCount(payment)

                    };

                    var success = await _payments.MakePayment(main, Token);
                    await Application.Current.MainPage.DisplayAlert("Subscription Created", $"{member.FullName} has been subscribed to {payment.Name} ", "OK");
                    IsBusy = false;
                    await _navigation.PopAsync();
                }
                else
                {
                    await Application.Current.MainPage.DisplayAlert("No connection", "Please check your connection ", "OK");
                    IsBusy = false;
                }
            }
            catch
            {

            }
           
        }

        public void YourGreeting(DateTime time)
        {

            if (time.Hour >= 0 && time.Hour < 12)
            {
                greeting = "Good Morning";
            }
            else if (time.Hour >= 12 && time.Hour < 18)
            {
                greeting = "Good Afternoon";
            }
            else if (time.Hour >= 18 && time.Hour <= 23)
            {
                greeting = "Good Evening";
            }

        }
    }
}
