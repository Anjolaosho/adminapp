﻿using AdminApp.Models;
using AdminApp.Models.PaymentsModel;
using AdminApp.MyInterfaces.IServices;
using AdminApp.MyInterfaces.IServices.Data;
using AdminApp.MyInterfaces.IServices.General;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace AdminApp.ViewModels.PaymentsViewModel
{
    class AddPaymentViewModel : BaseViewModel
    {
        private readonly IPayments _payments;
        protected readonly IConnectionService _connectionService;

        private readonly string Token;

        public INavigation _navigation { get; set; }

        private string _name;

        private long _amount;

        private DateTimeOffset _dateCreated;

        private Cycle _billingCycle;

        private Pay _paymentType;

        public string Interval { get; set; }

        public AddPaymentViewModel(IConnectionService connectionService, INavigation Navigation, IPayments payments, string authenticationResponse)
        {
            _connectionService = connectionService;
            _navigation = Navigation;
            _payments = payments;
            Token = authenticationResponse;
            YourGreeting(DateTime.Now);
        }
        public string Name
        {
            get { return _name; }
            set { SetProperty(ref _name, value); }
        }

        public long Amount
        {
            get { return _amount; }
            set { SetProperty(ref _amount, value); }
        }

        public DateTimeOffset DateCreated
        {
            get { return _dateCreated; }
            set { SetProperty(ref _dateCreated, value); }
        }

        bool notapplicable, weekly, monthly, biannually, annually;

       public bool Notapplicable
        {
            get { return notapplicable; }
            set { SetProperty(ref notapplicable, value);
                  if(notapplicable == true)
                {
                    Annually = false;
                    Biannually = false;
                    Monthly = false;
                    Weekly = false;
                    _billingCycle = Cycle.notapplicable;
                  }  
            }
        }

        public bool Annually
        {
            get { return annually; }
            set
            {
                SetProperty(ref annually, value);
                if (annually == true)
                {
                    Notapplicable = false;
                    Biannually = false;
                    Monthly = false;
                    Weekly = false;
                    _billingCycle = Cycle.annually;
                }
            }
        }

        public bool Biannually
        {
            get { return biannually; }
            set
            {
                SetProperty(ref biannually, value);
                if (biannually == true)
                {
                    Annually = false;
                    Notapplicable = false;
                    Monthly = false;
                    Weekly = false;
                    _billingCycle = Cycle.biannually;
                }
            }
        }

        public bool Monthly
        {
            get { return monthly; }
            set
            {
                SetProperty(ref monthly, value);
                if (monthly == true)
                {
                    Annually = false;
                    Biannually = false;
                    Notapplicable = false;
                    Weekly = false;
                    _billingCycle = Cycle.monthly;
                }
            }
        }

        public bool Weekly
        {
            get { return weekly; }
            set
            {
                SetProperty(ref weekly, value);
                if (weekly == true)
                {
                    Annually = false;
                    Biannually = false;
                    Monthly = false;
                    Notapplicable = false;
                    _billingCycle = Cycle.weekly;
                }
            }
        }

        public Cycle BillingCycle
        {
            get { return _billingCycle; }
            set { SetProperty(ref _billingCycle, value);}
            
        }


        bool onetime, recurring;

        public bool OneTime
        {
            get { return onetime; }
            set
            {
                SetProperty(ref onetime, value);
                if (onetime == true)
                {
                    Reoccurent = false;
                    _paymentType = Pay.onetime;
                }
            }
        }

        public bool Reoccurent
        {
            get { return recurring; }
            set
            {
                SetProperty(ref recurring, value);
                if (recurring == true)
                {
                    OneTime = false;
                    _paymentType = Pay.recurring;
                }
            }
        }

        public Pay PaymentType
        {
            get { return _paymentType; }
            set { SetProperty(ref _paymentType, value); }
        }

        public ICommand Back => new Command(BackTapped);

        private void BackTapped()
        {
            _navigation.PopAsync();
        }

        public ICommand sendcommand => new Command(Send);

        private async void Send()
        {
            try
            {
                if (_connectionService.IsConnected)
                {
                    if(_name != null && Amount != 0)
                    {
                        try
                        {
                            IsBusy = true;
                            await Application.Current.MainPage.DisplayAlert("Creating plan", $" Your Payment Plan will be sent shortely", "OK");

                            AddPayment addpayment = new AddPayment()
                            {
                                Name = _name,
                                Amount = _amount,
                                BillingCycle = _billingCycle,
                                PaymentType = _paymentType,
                                DateCreated = _dateCreated,
                                Interval = Interval,
                            };

                            var eventadded = await _payments.payments(addpayment, Token);
                            IsBusy = false;
                            await Application.Current.MainPage.DisplayAlert("Payment plan Created", "Payment Successfully added ", "OK");
                            await _navigation.PopAsync();
                        }
                        catch (Exception)
                        {
                            IsBusy = false;
                            await Application.Current.MainPage.DisplayAlert("Invalid Input", "Payment plan not Successfully added ", "OK");
                        }
                      
                    }
                    else
                    {
                        IsBusy = false;
                        await Application.Current.MainPage.DisplayAlert("Payment plan Created", "Some important fields are empty ", "OK");
                    }
                

                }
                else
                {
                    IsBusy = false;
                    await Application.Current.MainPage.DisplayAlert("Check your connection", "Please Check Network Connection ", "OK");
                }

            }
            catch
            {
             
            }

        }

        private string greeting;
        public string Greeting
        {
            get { return greeting; }
            set { SetProperty(ref greeting, value); }
        }

        public void YourGreeting(DateTime time)
        {

            if (time.Hour >= 0 && time.Hour < 12)
            {
                greeting = "Good Morning";
            }
            else if (time.Hour >= 12 && time.Hour < 18)
            {
                greeting = "Good Afternoon";
            }
            else if (time.Hour >= 18 && time.Hour <= 23)
            {
                greeting = "Good Evening";
            }

        }
    }
}
