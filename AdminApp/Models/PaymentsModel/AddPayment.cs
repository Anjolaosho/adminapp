﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace AdminApp.Models.PaymentsModel
{
    public enum Cycle { notapplicable, weekly, monthly, biannually, annually }
    public enum Pay { onetime, recurring }
    public partial class AddPayment
    {
        //[JsonProperty("id")]
        //public string Id { get; set; }

        //[JsonProperty("tenantId")]
        //public string TenantId { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("amount")]
        public long Amount { get; set; }

        [JsonProperty("dateCreated")]
        public DateTimeOffset DateCreated { get; set; }

        [JsonProperty("billingCycle")]
        public Cycle BillingCycle { get; set; }

        [JsonProperty("paymentType")]
        public Pay PaymentType { get; set; }

        [JsonProperty("paystackPlanCode")]
        public string PaystackPlanCode { get; set; }

        [JsonProperty("interval")]
        public string Interval { get; set; }
    }
}
