﻿using Akavache.Sqlite3.Internal;
using GalaSoft.MvvmLight;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace AdminApp.Models.PaymentsModel
{
    [Table("MyPayment")]
    public class Payment : ObservableObject
    {
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("tenantId")]
        public string TenantId { get; set; }

        [JsonProperty("amount")]
        public decimal Amount { get; set; }

        [JsonProperty("dateCreated")]
        public DateTimeOffset DateCreated { get; set; }

        [JsonProperty("billingCycle")]
        public Cycle BillingCycle { get; set; }

        [JsonProperty("paymentType")]
        public Pay PaymentType { get; set; }

        [JsonProperty("paystackPlanCode")]
        public string PaystackPlanCode { get; set; }

        [JsonProperty("isDeleted")]
        public bool IsDeleted { get; set; }

        //[JsonProperty("subscriptions")]
        //public List<Subscription> Subscriptions { get; set; }
    }

    public class MainSubscription
    {
        public int ID { get; set; }

        [JsonProperty("id")]
        public Guid Id { get; set; }

        [JsonProperty("paymentId")]
        public Guid PaymentId { get; set; }

        [JsonProperty("memberId")]
        public Guid MemberId { get; set; }

        [JsonProperty("memberImage")]
        public Uri MemberImage { get; set; }

        [JsonProperty("memberName")]
        public string MemberName { get; set; }

        [JsonProperty("paymentName")]
        public string PaymentName { get; set; }

        [JsonProperty("transactionDate")]
        public DateTimeOffset TransactionDate { get; set; }

        [JsonProperty("expiryDate")]
        public DateTimeOffset ExpiryDate { get; set; }

        [JsonProperty("amountPaid")]
        public long AmountPaid { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("reference")]
        public object Reference { get; set; }

        [JsonProperty("daysToExpire")]
        public long DaysToExpire { get; set; }

        [JsonProperty("daysAfterExpiry")]
        public long DaysAfterExpiry { get; set; }

        [JsonProperty("isActive")]
        public bool IsActive { get; set; }
    }

    public partial class PaymentRecords
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("memberId")]
        public string MemberId { get; set; }

        [JsonProperty("tenantId")]
        public string TenantId { get; set; }

        [JsonProperty("paymentPlanName")]
        public string PaymentPlanName { get; set; }

        [JsonProperty("amountPaid")]
        public long AmountPaid { get; set; }

        [JsonProperty("channel")]
        public string Channel { get; set; }

        [JsonProperty("datePaid")]
        public DateTimeOffset DatePaid { get; set; }

        [JsonProperty("isSuccessful")]
        public bool IsSuccessful { get; set; }
    }

    public class Subscription
    {
        //[PrimaryKey, AutoIncrement]
        //public int ID { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("tenantId")]
        public string TenantId { get; set; }

        [JsonProperty("memberName")]
        public string MemberName { get; set; }

        [JsonProperty("paymentName")]
        public string PaymentName { get; set; }

        [JsonProperty("transactionDate")]
        public DateTime TransactionDate { get; set; }

        [JsonProperty("expiryDate")]
        public DateTime ExpiryDate { get; set; }

        [JsonProperty("amountPaid")]
        public decimal AmountPaid { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("reference")]
        public string Reference { get; set; }

        [JsonProperty("isActive")]
        public bool IsActive { get; set; }
    }

    public class MainSub
    {
        [JsonProperty("MemberId")]
        public string MemberId { get; set; }

        [JsonProperty("PaymentId")]
        public string PaymentId { get; set; }

        [JsonProperty("SubscriptionCount")]
        public int subscriptionCount { get; set; }

        [JsonProperty("Reference")]
        public string Reference { get; set; }

        [JsonProperty("AmountPaid")]
        public decimal AmountPaid { get; set; }

        [JsonProperty("IsActive")]
        public bool isActive { get; set; }
    }

}
