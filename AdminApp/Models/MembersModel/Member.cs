﻿using AdminApp.Models.PaymentsModel;
using Akavache.Sqlite3.Internal;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Text;

namespace AdminApp.Models.MembersModel
{
    public enum Gender
    {
        Male,
        female
    }
    public class Member
    {
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("tenantId")]
        public string TenantId { get; set; }

        [JsonProperty("applicationUserId")]
        public string ApplicationUserId { get; set; }

        [JsonProperty("fullName")]
        public string FullName { get; set; }

        [JsonProperty("profilePicturePath")]
        public string ProfilePicturePath { get; set; }

        [JsonProperty("profilePictureBlobUrl")]
        public Uri ProfilePictureBlobUrl { get; set; }

        [JsonProperty("emailAddress")]
        public string EmailAddress { get; set; }

        [JsonProperty("phoneNumber")]
        public string PhoneNumber { get; set; }

        [JsonProperty("gender")]
        public Gender Gender { get; set; }

        [JsonProperty("residentialAddress")]
        public string ResidentialAddress { get; set; }

        [JsonProperty("birthday")]
        public DateTimeOffset Birthday { get; set; }

        [JsonProperty("wedding")]
        public DateTimeOffset Wedding { get; set; }

        [JsonProperty("dateJoined")]
        public DateTimeOffset DateJoined { get; set; }

        [JsonProperty("isActive")]
        public bool IsActive { get; set; }

        [JsonProperty("dateDeactivated")]
        public string DateDeactivated { get; set; }

        [JsonProperty("isDeleted")]
        public bool IsDeleted { get; set; }

        [JsonProperty("tenant")]
        public Tenant Tenant { get; set; }

        [JsonProperty("subscriptions")]
        public MainSubscription Subscriptions { get; set; }

        [JsonProperty("paymentRecords")]
        public PaymentRecords PaymentRecords { get; set; }
    }
                
       


    public class AddMember
    {
        [JsonProperty("fullName")]
        public string FullName { get; set; }

        [JsonProperty("username")]
        public string Username { get; set; }

        [JsonProperty("profilePicturePath")]
        public string ProfilePicturePath { get; set; }

        [JsonProperty("profilePictureBlobUrl")]
        public string ProfilePictureBlobUrl { get; set; }

        [JsonProperty("image")]
        public Image Image { get; set; }

        [JsonProperty("emailAddress")]
        public string EmailAddress { get; set; }

        [JsonProperty("phoneNumber") ]
        public string PhoneNumber { get; set; }

        [JsonProperty("gender")]
        public string Gender { get; set; }

        [JsonProperty("residentialAddress")]
        public string ResidentialAddress { get; set; }

        [JsonProperty("birthday")]
        public DateTimeOffset Birthday { get; set; }

        [JsonProperty("wedding")]
        public DateTimeOffset Wedding { get; set; }

        [JsonProperty("dateJoined")]
        public DateTimeOffset DateJoined { get; set; }


    }

    public class AMember
    {
    [JsonProperty("id")]
    public string Id { get; set; }

    [JsonProperty("tenantId")]
    public string TenantId { get; set; }

    [JsonProperty("applicationUserId")]
    public string ApplicationUserId { get; set; }

    [JsonProperty("fullName")]
    public string FullName { get; set; }

    [JsonProperty("profilePicturePath")]
    public string ProfilePicturePath { get; set; }

    [JsonProperty("profilePictureBlobUrl")]
    public string ProfilePictureBlobUrl { get; set; }

    [JsonProperty("emailAddress")]
    public string EmailAddress { get; set; }

    [JsonProperty("phoneNumber")]
    public string PhoneNumber { get; set; }

    [JsonProperty("gender")]
    public Gender Gender { get; set; }

    [JsonProperty("residentialAddress")]
    public string ResidentialAddress { get; set; }

    [JsonProperty("birthday")]
    public DateTimeOffset Birthday { get; set; }

    [JsonProperty("wedding")]
    public DateTime Wedding { get; set; }

    [JsonProperty("dateJoined")]
    public DateTime DateJoined { get; set; }

    [JsonProperty("isActive")]
    public bool IsActive { get; set; }

    [JsonProperty("dateDeactivated")]
    public DateTimeOffset DateDeactivated { get; set; }

    [JsonProperty("isDeleted")]
    public bool IsDeleted { get; set; }

    [JsonProperty("subscriptions")]
    public List<Subscription> Subscriptions { get; set; }

    [JsonProperty("paymentRecords")]
    public List<PaymentRecords> PaymentRecords { get; set; }
}

}


