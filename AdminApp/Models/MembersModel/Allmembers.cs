﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Text;

namespace AdminApp.Models.MembersModel
{
    //Get All members in the orgainzation
    public class AllMembers : ObservableObject
    {   
        public int ID { get; set; }
        public string Name { get; set; }
        public string TimeLeft { get; set; }
        public string Email { get; set; }
        public string PhoneNo { get; set; }
        public string address { get; set; }
        public string picturepath { get; set; }
    }
}
