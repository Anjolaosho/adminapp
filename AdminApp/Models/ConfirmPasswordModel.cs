﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace AdminApp.Models
{
    class ConfirmPasswordModel
    {
        [JsonProperty("username")]
        public string Username { get; set; }

        [JsonProperty("newPassword")]
        public string NewPassword { get; set; }

        [JsonProperty("manualGeneratedCode")]
        public string ManualGeneratedCode { get; set; }
    }
}
