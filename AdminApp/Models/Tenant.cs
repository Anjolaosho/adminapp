﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using AdminApp.Models.MembersModel;
using AdminApp.Models.PaymentsModel;
using AdminApp.Models.EventsModel;
using System.IO;
using System.Net.Http;
using AdminApp.Models.NoticeModel;

namespace AdminApp.Models
{
    public class Tenant
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("accountNumber")]
        public string AccountNumber { get; set; }

        [JsonProperty("bank")]
        public string Bank { get; set; }

        [JsonProperty("percentageCharge")]
        public long PercentageCharge { get; set; }

        [JsonProperty("subAccountCode")]
        public string SubAccountCode { get; set; }

        [JsonProperty("applicationUserId")]
        public string ApplicationUserId { get; set; }

        [JsonProperty("dateEstablished")]
        public DateTimeOffset DateEstablished { get; set; }

        [JsonProperty("logoPath")]
        public string LogoPath { get; set; }

        [JsonProperty("logoBlobUrl")]
        public string LogoBlobUrl { get; set; }

        [JsonProperty("openTime")]
        public DateTimeOffset OpenTime { get; set; }

        [JsonProperty("address")]
        public string Address { get; set; }

        [JsonProperty("tenantUsers")]
        public List<TenantUser> TenantUsers { get; set; }

        [JsonProperty("members")]
        public List<Member> Members { get; set; }

        [JsonProperty("payments")]
        public List<Payment> Payments { get; set; }

        [JsonProperty("events")]
        public List<Event> Events { get; set; }

        [JsonProperty("notices")]
        public List<object> Notices { get; set; }
    }

    public partial class TenantUser
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("photoPath")]
        public string PhotoPath { get; set; }

        [JsonProperty("photoBlobUrl")]
        public string PhotoBlobUrl { get; set; }

        [JsonProperty("tenantId")]
        public string TenantId { get; set; }

        [JsonProperty("applicationUserId")]
        public string ApplicationUserId { get; set; }
    }

    public class UpdateTenant
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("nameOfOrganization")]
        public string Name { get; set; }

        [JsonProperty("accountNumber")]
        public string AccountNumber { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("bank")]
        public string Bank { get; set; }

        [JsonProperty("percentageCharge")]
        public long PercentageCharge { get; set; }

        [JsonProperty("subAccountCode")]
        public string SubAccountCode { get; set; }

        [JsonProperty("applicationUserId")]
        public string ApplicationUserId { get; set; }

        [JsonProperty("dateEstablished")]
        public DateTime DateEstablished { get; set; }

        [JsonProperty("logoPath")]
        public string LogoPath { get; set; }

        [JsonProperty("logoBlobUrl")]
        public string LogoBlobUrl { get; set; }

        [JsonProperty("openTime")]
        public DateTime OpenTime { get; set; }

        [JsonProperty("address")]
        public string Address { get; set; }

        [JsonProperty("tenantUsers")]
        public MyTenantUser TenantUsers { get; set; }

        [JsonProperty("members")]
        public ICollection<Member> Members { get; set; }

        [JsonProperty("payments")]
        public ICollection<Payment> Payments { get; set; }

        [JsonProperty("events")]
        public ICollection<Events> Events { get; set; }

        [JsonProperty("notices")]
        public ICollection<Notices> Notices { get; set; }

    }

    public class MyTenantUser
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string PhotoPath { get; set; }
        public string PhotoBlobUrl { get; set; }
        public string TenantId { get; set; }
        public virtual Tenant Tenant { get; set; }
        public string ApplicationUserId { get; set; }
    }

}
