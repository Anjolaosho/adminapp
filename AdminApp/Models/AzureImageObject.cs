﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AdminApp.Models
{
    public class AzureImageObject
    {
        public string pictureName { get; set; }
        public string picturePath { get; set; }
    }
}
