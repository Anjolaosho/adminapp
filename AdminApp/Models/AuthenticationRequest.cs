﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace AdminApp.Models
{
    class AuthenticationRequest
    {
        [JsonProperty("nameOfOrganization")]
        public string NameOfOrganization { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("password")]
        public string Password { get; set; }
    }
}
