﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace AdminApp.Models
{
    public class RegisterTenant
    {
        [JsonProperty("nameOfOrganization")]
        public string NameOfOrganization { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("password")]
        public string Password { get; set; }

        [JsonProperty("accountNumber")]
        public string AccountNumber { get; set; }

        [JsonProperty("bank")]
        public string Bank { get; set; }

        [JsonProperty("dateEstablished")]
        public DateTimeOffset DateEstablished { get; set; }

        [JsonProperty("image")]
        public Image Image { get; set; }

        [JsonProperty("logoPath")]
        public string LogoPath { get; set; }

        [JsonProperty("logoBlobUrl")]
        public string LogoBlobUrl { get; set; }

        [JsonProperty("openTime")]
        public DateTimeOffset OpenTime { get; set; }

        [JsonProperty("address")]
        public string Address { get; set; }
    }

    public partial class Image
    {
        public string imagepath { get; set; }
    }

    public class LoginDTO
    {
        [JsonProperty("username")]
        public string Username { get; set; }

        [JsonProperty("password")]
        public string Password { get; set; }
    }

    public class TenantProfile
    {
        [JsonProperty("name")]
        public string NameOfOrganization { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("password")]
        public string Password { get; set; }

        [JsonProperty("accountNumber")]
        public string AccountNumber { get; set; }

        [JsonProperty("bank")]
        public string Bank { get; set; }

        [JsonProperty("dateEstablished")]
        public DateTime DateEstablished { get; set; }

        [JsonProperty("image")]
        public Image Image { get; set; }

        [JsonProperty("logoPath")]
        public string LogoPath { get; set; }

        [JsonProperty("logoBlobUrl")]
        public string LogoBlobUrl { get; set; }

        [JsonProperty("openTime")]
        public DateTime OpenTime { get; set; }

        [JsonProperty("address")]
        public string Address { get; set; }
    }
}
