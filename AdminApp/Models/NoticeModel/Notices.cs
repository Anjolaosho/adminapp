﻿using Akavache.Sqlite3.Internal;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace AdminApp.Models.NoticeModel
{
    public class Notices
    {

        //[JsonProperty("id")]
        //public string Id { get; set; }

        //[JsonProperty("tenantId")]
        //public string TenantId { get; set; }

        [JsonProperty("subject")]
        public string Subject { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }

        //[JsonProperty("datesent")]
        //public DateTime DateSent { get; set; }

        //[JsonProperty("tenant")]
        //public Tenant Tenant { get; set; }
    }

    public class NoticesDB
    {
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }

        //[JsonProperty("id")]
        //public string Id { get; set; }

        //[JsonProperty("tenantId")]
        //public string TenantId { get; set; }

        [JsonProperty("subject")]
        public string Subject { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }

        //[JsonProperty("datesent")]
        //public DateTime DateSent { get; set; }

        //[JsonProperty("tenant")]
        //public Tenant Tenant { get; set; }
    }
    public class YourNotice
    {

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("tenantId")]
        public string TenantId { get; set; }

        [JsonProperty("subject")]
        public string Subject { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("datesent")]
        public DateTime DateSent { get; set; }

        [JsonProperty("tenant")]
        public Tenant Tenant { get; set; }
    }
}

