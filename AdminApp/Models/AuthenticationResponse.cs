﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace AdminApp.Models
{
    public class AuthenticationResponse
    {
        [JsonProperty("token")]
        public string Token { get; set; }

        [JsonProperty("expiration")]
        public DateTimeOffset Expiration { get; set; }

        [JsonProperty("username")]
        public string Username { get; set; }

        [JsonProperty("userRoles")]
        public string UserRoles { get; set; }
    }
}

