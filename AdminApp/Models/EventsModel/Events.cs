﻿using Akavache.Sqlite3.Internal;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;

namespace AdminApp.Models.EventsModel
{
    public class Event
    {
        //[JsonProperty("id")]
        //public string Id { get; set; }

        //[JsonProperty("tenantId")]
        //public string TenantId { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("bannerPath")]
        public string BannerPath { get; set; }

        [JsonProperty("bannerBlobUrl")]
        public string BannerBlobUrl { get; set; }             

        [JsonProperty("details")]
        public string Details { get; set; }

        [JsonProperty("venue")]
        public string Venue { get; set; }

        [JsonProperty("mapUrl")]
        public string MapUrl { get; set; }

        [JsonProperty("startTime")]
        public DateTime StartTime { get; set; }

        [JsonProperty("endTime")]
        public DateTime EndTime { get; set; }
    }

    public class Events
    {
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("tenantId")]
        public string TenantId { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("bannerPath")]
        public string BannerPath { get; set; }

        [JsonProperty("bannerBlobUrl")]
        public string BannerBlobUrl { get; set; }

        [JsonProperty("details")]
        public string Details { get; set; }

        [JsonProperty("venue")]
        public string Venue { get; set; }

        [JsonProperty("mapUrl")]
        public string MapUrl { get; set; }

        [JsonProperty("startTime")]
        public DateTime StartTime { get; set; }

        [JsonProperty("endTime")]
        public DateTime EndTime { get; set; }
    }
}
