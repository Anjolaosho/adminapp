﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Text;

namespace AdminApp.Models.EventsModel
{
    class AllEvents : ObservableObject
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string TimeLeft { get; set; }
    }
}
