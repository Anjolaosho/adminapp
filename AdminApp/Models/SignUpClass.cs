﻿using MvvmHelpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace AdminApp.Models
{
    public class SignUpClass : ObservableObject 
    {
        public string Id { get; set; }
        public string UserId { get; set; }

        private string Email;
        public string email { get { return Email; } set { SetProperty(ref Email, value); } }

        string Password;
        public string password { get { return Password; } set { SetProperty(ref Password, value); } }

        string nameoforg;
        public string NameofOrg { get { return nameoforg; } set { SetProperty(ref nameoforg, value); } }
    }
}
