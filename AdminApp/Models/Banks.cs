﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;

namespace AdminApp.Models
{
    public class Banks
    {
        public bool status { get; set; }
        public string message { get; set; }

        public List<BankData> data { get; set; }

    }

    public class BankData
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("code")]
        public string Code { get; set; }
    }
}



